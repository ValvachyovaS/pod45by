package itstep.valva4eva.command;

import javax.servlet.ServletException;
import java.io.IOException;
import java.sql.SQLException;

import itstep.valva4eva.entity.User;
import itstep.valva4eva.service.UserService;
import itstep.valva4eva.tool.Crypt;

public class RegisterCommand extends FrontCommand {

	@Override
	public void process() throws ServletException, IOException {
		try {
			String erMsg;
			UserService us = new UserService(ds);
			if (validForm()) {
				String tel = request.getParameter("tel1") + request.getParameter("tel2");
				String encr = Crypt.getInstance().encrypt(request.getParameter("pas"));
				User user = new User(tel, encr, 
						request.getParameter("nam"),
						request.getParameter("eml"));
				if (us.findByTel(tel) == null) { //если пользователя таким телефоном нет в бд
					if (request.getParameter("pas").equals(request.getParameter("pas2"))) { //совпадение паролей
						String captcha = request.getSession().getAttribute("captcha").toString();
						if (request.getParameter("code").equalsIgnoreCase(captcha)) { //если капча введена верно
							System.out.println("code->" + request.getParameter("code") + " captcha->" + captcha);
							int i = us.create(user);
							if (i == 1) {
								System.out.println("Пользователь создан!");
								request.getSession().setAttribute("reguser", us.findByTel(tel));
								forward("index");
								return;
							} else {
								erMsg = "Ошибка сервера! Пользователь не может быть зарегистрирован.";
								System.out.println(erMsg);
								request.setAttribute("errCaptcha", erMsg);
							}
						} else {
							erMsg = "Код с картинки введен неверно!";
							System.out.println(erMsg);
							request.setAttribute("errCaptcha", erMsg);
						}
					} else {
						erMsg = "Пароли не совпадают!";
						System.out.println(erMsg);
						request.setAttribute("errPassword", erMsg);
					}
				} else {
					erMsg = "Пользователь с телефоном +375 "+tel+" уже существует!";
					System.out.println(erMsg);
					request.setAttribute("errTelefon", erMsg);
				}
				writeInput(user); //в случае любой ошибки - сохраннение значений полей в запросе
			}
			forward("register");
		} catch (IllegalArgumentException i) {
			System.out.println("Error IllegalArgumentException from RegisterCommand");
			i.printStackTrace();
			request.setAttribute("errRegister", "Ошибка шифрования пароля! IllegalArgumentException from RegisterCommand: " + i.getMessage());
			forward("register");
		} catch (SQLException s) {
			System.out.println("Error SQLException from RegisterCommand");
			s.printStackTrace();
			request.setAttribute("errRegister", "Ошибка соединения с сервером! SQLException: " + s.getMessage());
			forward("register");
		} catch (Exception e) {
			System.out.println("Error Exception from RegisterCommand");
			e.printStackTrace();
			request.setAttribute("errRegister", "Произошла ошибка! Exception: " + e.getMessage());
			forward("register");
		}

	}
	
	private boolean validForm() {
		if (request.getParameter("tel1") != null 
				&& request.getParameter("tel2") != null
				&& request.getParameter("pas") != null
				&& request.getParameter("pas2") != null
				&& request.getParameter("code") != null) {
			return true;
		}
		return false;
	}
	
	private void writeInput(User user) {
		request.setAttribute("valueTel1", user.getTel().substring(0, 2));
		request.setAttribute("valueTel2", user.getTel().substring(2, 9));
		request.setAttribute("valueNam", user.getNam());
		request.setAttribute("valueEml", user.getEml());
	}
}
