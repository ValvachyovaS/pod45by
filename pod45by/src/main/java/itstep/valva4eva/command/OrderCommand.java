package itstep.valva4eva.command;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import itstep.valva4eva.command.FrontCommand;
import itstep.valva4eva.entity.Order;
import itstep.valva4eva.entity.Ordrow;
import itstep.valva4eva.entity.Serv;
import itstep.valva4eva.entity.User;
import itstep.valva4eva.service.OrderService;
import itstep.valva4eva.service.ServService;
import itstep.valva4eva.tool.Mailer;

public class OrderCommand extends FrontCommand {

	@Override
	public void process() throws ServletException, IOException {
		try {
		if(loginUser()) {
			HttpSession session = request.getSession();
			if (session.getAttribute("listService") == null) {
				ServService ss = new ServService(ds);
				List<Serv> listService = ss.readAct(1);
				session.setAttribute("listService", listService);
				List<Ordrow> listOrdrow = new ArrayList<>();
				session.setAttribute("listOrdrow", listOrdrow);
				int i = 0;
				session.setAttribute("identity", i);
			}

			if (request.getParameter("service-id") != null && request.getParameter("count") != null) {
				@SuppressWarnings("unchecked")
				List<Ordrow> listOrdrow = (List<Ordrow>) session.getAttribute("listOrdrow");
				int i = (int) session.getAttribute("identity");
				int serviceId = Integer.parseInt(request.getParameter("service-id"));
				int count = Integer.parseInt(request.getParameter("count"));
				Optional<Ordrow> opt = listOrdrow.stream().filter(o -> o.getService_id() == serviceId).findFirst();
				Ordrow findOrd = null;
				if (opt.isPresent()) {
					findOrd = opt.get();
				}
				
				if (findOrd == null) { //new
					Ordrow ord = new Ordrow();
					ServService ss = new ServService(ds);
					Serv serv = ss.findById(serviceId);
					ord.setService_id(serv.getId());
					ord.setName(serv.getName());
					ord.setRate(serv.getRate());
					ord.setCount(count);
					ord.setId(i);
					i++;
					listOrdrow.add(ord);
				} else { //replace
					listOrdrow.remove(findOrd);
					findOrd.setCount(findOrd.getCount() + count);
					listOrdrow.add(findOrd);
				}
				session.setAttribute("identity", i);
				session.setAttribute("listOrdrow", listOrdrow);
				request.setAttribute("service-id", null);
				request.setAttribute("count", null);
			}

			if (request.getParameter("ordrow-id") != null) {
				@SuppressWarnings("unchecked")
				List<Ordrow> listOrdrow = (List<Ordrow>) session.getAttribute("listOrdrow");
				int ordrowId = Integer.parseInt(request.getParameter("ordrow-id"));
				Optional<Ordrow> opt = listOrdrow.stream().filter(o -> o.getId() == ordrowId).findAny();
				Ordrow findOrd = null;
				if (opt.isPresent()) {
					findOrd = opt.get();
				}
				if (findOrd != null) {
					listOrdrow.remove(findOrd);
					session.setAttribute("listOrdrow", listOrdrow);
				}
				request.removeAttribute("ordrow-id");
			}

			if (request.getParameter("clear") != null) {
				List<Ordrow> listOrdrow = new ArrayList<>();
				session.setAttribute("listOrdrow", listOrdrow);
				int i = 0;
				session.setAttribute("identity", i);
				request.removeAttribute("clear");
			}

			if (request.getParameter("orderforma") != null) {
				@SuppressWarnings("unchecked")
				List<Ordrow> listOrdrow = (List<Ordrow>) session.getAttribute("listOrdrow");
				User user = (User) session.getAttribute("reguser");
				OrderService os = new OrderService(ds);
				String adr = "";
				if (request.getParameter("adr") != null) {
					adr = request.getParameter("adr");
				}
				String comment = "";
				if (request.getParameter("comment") != null) {
					comment = request.getParameter("comment");
				}
				Order order = new Order(adr, comment, user.getId());
				int order_id = os.create(order);
				order = os.findById(order_id);
				session.setAttribute("neworder", order);

				if (listOrdrow != null && !listOrdrow.isEmpty()) {
					for (Ordrow ord : listOrdrow) {
						ord.setId(0);
						ord.setOrder_id(order.getId());
						os.createRow(ord);
					}
				}
				ResourceBundle mailProps = ResourceBundle.getBundle("mail");
				Mailer.send("Поступила заявка!", 
						Mailer.textOrder(user, order, listOrdrow), 
						mailProps.getObject("to").toString());
				request.removeAttribute("orderforma");
				session.removeAttribute("listService");
				session.removeAttribute("listOrdrow");
				session.removeAttribute("identity");
				session.removeAttribute("neworder");

				request.setAttribute("msgOrder",
						"Заявка успешно отправлена! В ближайшее рабочее время мастер свяжется с Вами по указанному телефону.");
			}
			forward("order");
		} else {
			redirect("login");
		}
		} catch (MessagingException m) {
			System.out.println("Error MessagingException from OrderCommand");
			m.printStackTrace();
			request.setAttribute("msgRing", "Ошибка службы почтовых уведомлений! MessagingException: " + m.getMessage());
			forward("order");
		} catch (SQLException s) {
			System.out.println("Error SQLException from OrderCommand");
			s.printStackTrace();
			request.setAttribute("msgOrder",
					"Ошибка соединения с сервером! SQLException: " + s.getMessage() + "[from OrderCommand]");
			forward("order");
		} catch (Exception e) {
			System.out.println("Error Exception from OrderCommand");
			e.printStackTrace();
			request.setAttribute("msgOrder", "Произошла ошибка! Exception: " + e.getMessage() + "[from OrderCommand]");
			forward("order");
		}
			
	}
}