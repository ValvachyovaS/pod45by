package itstep.valva4eva.command;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.ServletException;

import itstep.valva4eva.command.FrontCommand;
import itstep.valva4eva.entity.Serv;
import itstep.valva4eva.service.ServService;

public class AdminServiceCommand extends FrontCommand {
	@Override
	public void process() throws ServletException, IOException {

		try {
			if (loginAdmin()) {
				ServService ss = new ServService(ds);
				List<Serv> listActServ = ss.readAct(1);
				request.setAttribute("listActServ", listActServ);
				List<Serv> listDeactServ = ss.readAct(0);
				request.setAttribute("listDeactServ", listDeactServ);
				
				if(request.getParameter("serv-act-btn") != null) {
					String btn = request.getParameter("serv-act-btn");
					if(request.getParameter("serv-id-a") != null && !"".equals(request.getParameter("serv-id-a"))) {
						int servId = Integer.parseInt(request.getParameter("serv-id-a"));
						Serv service = ss.findById(servId);
						int i = listActServ.indexOf(service);
						switch(btn) {
						case "copy":
							request.setAttribute("servname", service.getName());
							request.setAttribute("servrate", service.getRate());
							break;
						case "deact":
							service.setAct(0);
							listActServ.remove(i);
							request.setAttribute("listActServ", listActServ);
							listDeactServ.add(service);
							request.setAttribute("listDeactServ", listDeactServ);
							ss.update(service);
							break;
						}
					}
				}
				
				if(request.getParameter("servname") != null && request.getParameter("servrate") != null ) {
					String name = request.getParameter("servname");
					double rate = Double.parseDouble(request.getParameter("servrate"));
					Serv serv = new Serv(name, rate);
					serv.setId(ss.create(serv));
					listActServ.add(serv);
					request.setAttribute("listActServ", listActServ);
					request.setAttribute("msgAdminServ", "Услуга добавлена.");
				}
				
				if(request.getParameter("serv-deact-btn") != null) {
					String btn = request.getParameter("serv-deact-btn");
					if(request.getParameter("serv-id-d") != null && !"".equals(request.getParameter("serv-id-d"))) {
						int servId = Integer.parseInt(request.getParameter("serv-id-d"));
						Serv service = ss.findById(servId);
						int i = listDeactServ.indexOf(service);
						switch(btn) {
						case "clear":
							if (service.getAct() == 0) {
							if (ss.delDeact(service.getId()) == 0) {
								request.setAttribute("msgAdminServ", "Услуга не может быть удалена, содержится в заявках.");	
							} else {
								listDeactServ = ss.readAct(0);
								request.setAttribute("listDeactServ", listDeactServ);
								request.setAttribute("msgAdminServ", "Услуга удалена.");
							}
							} else {
								request.setAttribute("msgAdminServ", "Услуга не может быть удалена, является активной.");
							}
							break;
						case "activ":
							service.setAct(1);
							listDeactServ.remove(i);
							request.setAttribute("listDeactServ", listDeactServ);
							listActServ.add(service);
							request.setAttribute("listActServ", listActServ);
							ss.update(service);
							break;
						}
					}
				}
				
				forward("admin-service");
			} else {
				redirect("index");
			}
		} catch (SQLException s) {
			System.out.println("Error SQLException from AdminOrderCommand");
			s.printStackTrace();
			request.setAttribute("msgAdminServ", "Ошибка соединения с сервером! SQLException: " + s.getMessage());
			forward("admin-service");
		} catch (Exception e) {
			System.out.println("Error Exception from AdminOrderCommand");
			e.printStackTrace();
			request.setAttribute("msgAdminServ", "Произошла ошибка! Exception: " + e.getMessage());
			forward("admin-service");
		}
	}
}
