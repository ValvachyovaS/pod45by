package itstep.valva4eva.command;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;

import itstep.valva4eva.command.FrontCommand;
import itstep.valva4eva.entity.Order;
import itstep.valva4eva.entity.User;
import itstep.valva4eva.service.OrderService;
import itstep.valva4eva.service.UserService;

public class AdminUserCommand extends FrontCommand {
	@Override
	public void process() throws ServletException, IOException {

		try {
			if (loginAdmin()) {
				UserService us = new UserService(ds);
				List<User> listUser = us.readAll();
				User userReg = (User) request.getSession().getAttribute("reguser");
				User userAdmin = us.findById(1);
				if(userAdmin != null) listUser.remove(userAdmin);
				listUser.remove(userReg);
				request.setAttribute("listUser", listUser);

				if (request.getParameter("tel2") != null ) {
					String tel = request.getParameter("tel1") + request.getParameter("tel2");
					User userFind = us.findByTel(tel);
					if (userFind != null) {
						List<User> listUser2 = new ArrayList<>();
						if (!userFind.equals(userReg) && !userFind.equals(userAdmin)) {
						listUser2.add(userFind);
						}
						request.setAttribute("listUser", listUser2);
					} else {
						request.setAttribute("msgAdminUser", "Пользователь по такому номеру телефона не найден!");
					}
				} 
				
				if (request.getParameter("submit-btn") != null ) {
					String btn = request.getParameter("submit-btn");
					if (request.getParameter("user-id") != null && !"".equals(request.getParameter("user-id"))) {
						int userId = Integer.parseInt(request.getParameter("user-id"));
						User user = us.findById(userId);
						int i = listUser.indexOf(user);
						switch(btn) {
						case "admin":
							user.setRol(9);
							listUser.set(i, user);
							us.update(user);
							break;
						case "user":
							user.setRol(1);
							listUser.set(i, user);
							us.update(user);
							break;
						case "lock":
							user.setRol(0);
							listUser.set(i, user);
							us.update(user);
							break;
						case "delete":
							OrderService os = new OrderService(ds);
							List<Order> listOrder = os.readByUserId(user.getId());
							for (Order ord : listOrder) {
								os.delete(ord);
							}
							if (us.delete(user) == 1) {
								listUser.remove(user);
								request.setAttribute("msgAdminUser", "Пользователь с номером +375 " + user.getTel() +" удален!");
							} else {
								request.setAttribute("msgAdminUser", "Не удалось удалить пользователя!");
							}
							break;
						default:
							request.setAttribute("listUser", listUser);
							response.sendRedirect(request.getHeader("referer"));
							break;
						}
					} else {
						request.setAttribute("msgAdminUser", "Вы не выбрали пользователя!");
					}
				} 
				forward("admin-user");
			} else {
				redirect("index");
			}
		} catch (SQLException s) {
			System.out.println("Error SQLException from AdminUserCommand");
			s.printStackTrace();
			request.setAttribute("msgAdminUser", "Ошибка соединения с сервером! SQLException: " + s.getMessage());
			forward("admin-user");
		} catch (Exception e) {
			System.out.println("Error Exception from AdminUserCommand");
			e.printStackTrace();
			request.setAttribute("msgAdminUser", "Произошла ошибка! Exception: " + e.getMessage());
			forward("admin-user");
		}
	}
}
