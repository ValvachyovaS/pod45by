package itstep.valva4eva.command;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import java.io.IOException;
import java.sql.SQLException;

import itstep.valva4eva.entity.User;
import itstep.valva4eva.service.UserService;
import itstep.valva4eva.tool.Crypt;
import itstep.valva4eva.tool.Mailer;

public class LosepasCommand extends FrontCommand {

	@Override
	public void process() throws ServletException, IOException {
		
		try {
			if (request.getParameter("tel1") != null 
					&& request.getParameter("tel2") != null) {
				String tel = request.getParameter("tel1") + request.getParameter("tel2");
				UserService us = new UserService(ds);
				User user = us.findByTel(tel);
				if(user != null) {
					String decr = Crypt.getInstance().decrypt(user.getPas());
					Mailer.send("Восстановление пароля", Mailer.textPas(decr) , user.getEml());
					request.setAttribute("msgLosepas", "Сообщение успешно отправлено. Проверьте почтовый ящик.");
				} else {
					request.setAttribute("msgLosepas", "Пользователя с номером +375 " + tel + "не существует.");
				}
			}
		} catch (IllegalArgumentException i) {
			System.out.println("Error IllegalArgumentException from LosepasCommand");
			i.printStackTrace();
			request.setAttribute("msgLosepas", "Ошибка дешифрования пароля! IllegalArgumentException from LoginCommand: " + i.getMessage());
		} catch (MessagingException m) {
			System.out.println("Error MessagingException from LosepasCommand");
			m.printStackTrace();
			request.setAttribute("msgLosepas", "Сообщение не отправлено! MessagingException: " + m.getMessage());
		} catch (SQLException s) {
			System.out.println("Error SQLException from LosepasCommand");
			s.printStackTrace();
			request.setAttribute("msgLosepas", "Ошибка соединения с сервером! SQLException: " + s.getMessage());
		} catch (Exception e) {
			System.out.println("Error Exception from LosepasCommand");
			e.printStackTrace();
			request.setAttribute("msgLosepas", "Произошла ошибка! Exception: " + e.getMessage());
		} finally {
			forward("losepas");
		}
	}
}
