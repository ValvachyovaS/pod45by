package itstep.valva4eva.command;

import javax.servlet.ServletException;
import java.io.IOException;

public class GetoutCommand extends FrontCommand {

	@Override
	public void process() throws ServletException, IOException {
		request.getSession().removeAttribute("reguser");
		redirect("index");
	}
}
