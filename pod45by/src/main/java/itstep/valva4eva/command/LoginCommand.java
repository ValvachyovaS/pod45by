package itstep.valva4eva.command;

import javax.servlet.ServletException;
import java.io.IOException;
import java.sql.SQLException;

import itstep.valva4eva.entity.User;
import itstep.valva4eva.service.UserService;
import itstep.valva4eva.tool.Crypt;

public class LoginCommand extends FrontCommand {

	@Override
	public void process() throws ServletException, IOException {
		try {
			String erMsg;
			UserService us = new UserService(ds);
			if (request.getParameter("tel1") != null && request.getParameter("tel2") != null
					&& request.getParameter("pas") != null) {
				String tel = request.getParameter("tel1") + request.getParameter("tel2");
				User user = us.findByTel(tel);
				if (user != null && user.numRol() != 0) {// если пользователь с таким телефоном есть в бд и не заблокирован
					String decr = Crypt.getInstance().decrypt(user.getPas());
					if (request.getParameter("pas").equals(decr)) { // проверка введенного пароля
						System.out.println(
								"Аутентификация пользователя с номером +375 " + user.getTel() + " прошла успешно!");
						request.getSession().setAttribute("reguser", user);
						forward("index");
						return;
					} else {
						erMsg = "Ошибка аутентификации: пароль введен неверно.";
						System.out.println(erMsg);
						request.setAttribute("errLog", erMsg);
					}
				} else {
					erMsg = "Ошибка аутентификации: пользователь не зарегистрирован либо заблокирован.";
					System.out.println(erMsg);
					request.setAttribute("errLog", erMsg);
				}
			}
			forward("login");
		} catch (IllegalArgumentException i) {
			System.out.println("Error IllegalArgumentException from LoginCommand");
			i.printStackTrace();
			request.setAttribute("errLog", "Ошибка дешифрования пароля! IllegalArgumentException from LoginCommand: " + i.getMessage());
			forward("login");
		} catch (SQLException s) {
			System.out.println("Error SQLException from LoginCommand");
			s.printStackTrace();
			request.setAttribute("errLog", "Ошибка соединения с сервером! SQLException: " + s.getMessage());
			forward("login");
		} catch (Exception e) {
			System.out.println("Error Exception from LoginCommand");
			e.printStackTrace();
			request.setAttribute("msgLog", "Произошла ошибка! Exception: " + e.getMessage());
			forward("login");
		}
	}
}
