package itstep.valva4eva.command;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ResourceBundle;

import itstep.valva4eva.entity.Ring;
import itstep.valva4eva.entity.User;
import itstep.valva4eva.service.RingService;
import itstep.valva4eva.tool.Mailer;

public class RingCommand extends FrontCommand {

	@Override
	public void process() throws ServletException, IOException {
		
		try {
			if (request.getParameter("tel1") != null 
					&& request.getParameter("tel2") != null
					&& request.getParameter("nam") != null) {
				Ring ring = new Ring(request.getParameter("tel1") + request.getParameter("tel2"), 
						request.getParameter("nam"));
				RingService rs = new RingService(ds);
				int i = rs.create(ring);
				String erMsg;
				if (i == 1) {
					ResourceBundle mailProps = ResourceBundle.getBundle("mail");
					Mailer.send("+375"+ring.getTel(), ring.getName(), mailProps.getObject("to").toString());
					erMsg = "Сообщение успешно отправлено.";
					System.out.println(erMsg);
					request.setAttribute("msgRing", erMsg);
				} else {
					erMsg = "Ошибка сервера! Запись не сохранена в бд.";
					System.out.println(erMsg);
					request.setAttribute("msgRing", erMsg);
				}
			}
			if(request.getSession().getAttribute("reguser") != null) {
				User user = (User) request.getSession().getAttribute("reguser");
				request.setAttribute("valueTel1", user.getTel().substring(0, 2));
				request.setAttribute("valueTel2", user.getTel().substring(2, 9));
				request.setAttribute("valueNam", user.getNam());
			}
		} catch (MessagingException m) {
			System.out.println("Error MessagingException from RingCommand");
			m.printStackTrace();
			request.setAttribute("msgRing", "Сообщение не отправлено! MessagingException: " + m.getMessage());
		} catch (SQLException s) {
			System.out.println("Error SQLException from RingCommand");
			s.printStackTrace();
			request.setAttribute("msgRing", "Ошибка соединения с сервером! SQLException: " + s.getMessage());
		} catch (Exception e) {
			System.out.println("Error Exception from RingCommand");
			e.printStackTrace();
			request.setAttribute("msgRing", "Произошла ошибка! Exception: " + e.getMessage());
		} finally {
			forward("ring");
		}
	}
}
