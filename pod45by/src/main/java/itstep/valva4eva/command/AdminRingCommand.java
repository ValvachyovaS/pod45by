package itstep.valva4eva.command;

import javax.servlet.ServletException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import itstep.valva4eva.command.FrontCommand;
import itstep.valva4eva.entity.Ring;
import itstep.valva4eva.service.RingService;

public class AdminRingCommand extends FrontCommand {

	@Override
	public void process() throws ServletException, IOException {
		try {
		if(loginAdmin()) {
			RingService rs = new RingService(ds);
			List<Ring> listRings;
			if(request.getParameter("dateclear") == null && request.getParameter("tel2") == null) {
				listRings = rs.readAll();
				request.setAttribute("listRings", listRings);
			}
			if (request.getParameter("dateclear") != null ) {
				String dc = request.getParameter("dateclear");
				rs.delDate(dc);
				listRings = rs.readAll();
				request.setAttribute("listRings", listRings);
			} 
			if (request.getParameter("tel2") != null ) {
				String tel = request.getParameter("tel1") + request.getParameter("tel2");
				listRings = rs.searchTel(tel);
				request.setAttribute("listRings", listRings);
			} 
			forward("admin-ring");
		} else {
			redirect("index");
		}
		} catch (SQLException s) {
			System.out.println("Error SQLException from AdminCommand");
			s.printStackTrace();
		} catch (Exception e) {
			System.out.println("Error Exception from AdminCommand");
			e.printStackTrace();
		}
	}
}
