package itstep.valva4eva.command;

import javax.servlet.ServletException;
import java.io.IOException;
import java.sql.SQLException;

import itstep.valva4eva.command.FrontCommand;

public class AdminCommand extends FrontCommand {

	@Override
	public void process() throws ServletException, IOException {
			try {
				if(loginAdmin()) {
					forward("admin");
				} else {
					redirect("index");
				}
			} catch (SQLException s) {
				System.out.println("Error SQLException from AdminCommand");
				s.printStackTrace();
				request.setAttribute("errLog", "Ошибка соединения с сервером! SQLException: " + s.getMessage());
				forward("error");
			}
	}
}
