package itstep.valva4eva.command;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.ServletException;

import itstep.valva4eva.command.FrontCommand;
import itstep.valva4eva.entity.Order;
import itstep.valva4eva.entity.User;
import itstep.valva4eva.service.OrderService;
import itstep.valva4eva.service.UserService;
import itstep.valva4eva.tool.Crypt;

public class AccountCommand extends FrontCommand {
	@Override
	public void process() throws ServletException, IOException {

		try {
			if (loginUser()) {
				User user = (User) request.getSession().getAttribute("reguser");
				OrderService os = new OrderService(ds);
				List<Order> listOrder = os.readByUserId(user.getId());
				for (Order ord : listOrder) {
					ord.setOrdrows(os.readAllRows(ord.getId()));
				}
				request.setAttribute("listOrder", listOrder);
				UserService us = new UserService(ds);
				boolean goUpd = false;
				
				
				String password1 = request.getParameter("pas");
				String password2 = request.getParameter("pas2");
				System.out.println(password1 + "<- пароли - >" + password2);
				if (password1 != null && password2 != null && 
						!"".equals(password1) && !"".equals(password2)) {
					request.setAttribute("password1", null);
					request.setAttribute("password2", null);
					if (password1.equals(password2)) {
						String encr = Crypt.getInstance().encrypt(password1);
						user.setPas(encr);
						System.out.println(encr);
						goUpd = true;
					} else {
						request.setAttribute("errPassword", "Пароли введены неверно! Должны совпадать.");
						forward("account");
						return;
					}
				}
				
				String name = request.getParameter("nam");
				String email = request.getParameter("eml");
				if (name != null && email != null && ((!user.getNam().equals(name) || !user.getEml().equals(email)))) {
					user.setNam(name);
					user.setEml(email);
					goUpd = true;
				}
				if (goUpd) {
					int i = us.update(user);
					if (i == 1) {
						request.getSession().setAttribute("reguser", user);
						request.setAttribute("msgAccount", "Регистрационные данные пользователя обновлены.");
						forward("account");
						return;
					} else {
						request.setAttribute("msgAccount",
								"Ошибка сервера! Данные пользователя не могут быть сохранены.");
						forward("account");
						return;
					}
				}
				forward("account");
			} else {
				redirect("login");
				return;
			}
		} catch (SQLException s) {
			System.out.println("Error SQLException from AccountCommand");
			s.printStackTrace();
			request.setAttribute("msgAccount", "Ошибка соединения с сервером! SQLException: " + s.getMessage());
			forward("account");
		} catch (Exception e) {
			System.out.println("Error Exception from AccountCommand");
			e.printStackTrace();
			request.setAttribute("msgAccount", "Произошла ошибка! Exception: " + e.getMessage());
			forward("account");
		}
	}
}
