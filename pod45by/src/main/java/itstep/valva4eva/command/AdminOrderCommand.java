package itstep.valva4eva.command;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import itstep.valva4eva.command.FrontCommand;
import itstep.valva4eva.entity.Order;
import itstep.valva4eva.entity.User;
import itstep.valva4eva.service.OrderService;
import itstep.valva4eva.service.UserService;

public class AdminOrderCommand extends FrontCommand {
	@Override
	public void process() throws ServletException, IOException {

		try {
			if (loginAdmin()) {
				UserService us = new UserService(ds);
				OrderService os = new OrderService(ds);
				HttpSession session = request.getSession();
				List<Order> listOrder;
				String status = "Оформлена";
				if(request.getParameter("radio-status") != null) {
					status = request.getParameter("radio-status");
				} else {
					if (session.getAttribute("rs") != null) status = (String) session.getAttribute("rs");
				}
				if (status.equals("Все")) {
					listOrder = os.readAll();
					session.setAttribute("rs", "Все");
				} else {
					listOrder = os.readByStatus(status);
					session.setAttribute("rs", status);
				}
				for (Order ord : listOrder) {
					ord.setUser(us.findById(ord.getUser_id()));
					ord.setOrdrows(os.readAllRows(ord.getId()));
				}
				request.setAttribute("listOrder", listOrder);
				
				//Поиск заявок по номеру телефона
				if (request.getParameter("tel2") != null ) {
					String tel = request.getParameter("tel1") + request.getParameter("tel2");
					User userFind = us.findByTel(tel);
					if (userFind != null) {
						listOrder = os.readByUserId(userFind.getId());
						
						for (Order ord : listOrder) {
							ord.setUser(userFind);
							ord.setOrdrows(os.readAllRows(ord.getId()));
						}
					} else {
						listOrder = Collections.emptyList();
						request.setAttribute("msgAdminOrder", "Заявки по такому номеру телефона не найдены!");
					}
					request.setAttribute("listOrder", listOrder);
					session.setAttribute("rs", null);
				} 
				
				if (request.getParameter("order-id-status") != null 
						&& !"".equals(request.getParameter("order-id-status"))) {
					int orderId = Integer.parseInt(request.getParameter("order-id-status"));
					Order order = os.findById(orderId);
					String paramStatus = request.getParameter("status");
					if (!paramStatus.equals(order.getStatus())) {
						listOrder.remove(order);
						order.setStatus(paramStatus);
						listOrder.add(order);
						request.setAttribute("listOrder", listOrder);
						response.sendRedirect(request.getHeader("referer"));
						os.update(order);
						return;
					} 
				}
				if (request.getParameter("order-id-discount") != null 
						&& !"".equals(request.getParameter("order-id-discount"))) {
					int orderId = Integer.parseInt(request.getParameter("order-id-discount"));
					int discount = Integer.parseInt(request.getParameter("discount"));
					Order order = os.findById(orderId);
					if (discount != order.getDiscount()) {
						listOrder.remove(order);
						order.setDiscount(discount);
						listOrder.add(order);
						request.setAttribute("listOrder", listOrder);
						response.sendRedirect(request.getHeader("referer"));
						os.update(order);
						return;
					}
				}
				forward("admin-order");
			} else {
				redirect("index");
			}
		} catch (SQLException s) {
			System.out.println("Error SQLException from AdminOrderCommand");
			s.printStackTrace();
			request.setAttribute("msgAdminOrder", "Ошибка соединения с сервером! SQLException: " + s.getMessage());
			forward("admin-order");
		} catch (Exception e) {
			System.out.println("Error Exception from AdminOrderCommand");
			e.printStackTrace();
			request.setAttribute("msgAdminOrder", "Произошла ошибка! Exception: " + e.getMessage());
			forward("admin-order");
		}
	}
}
