package itstep.valva4eva.command;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;

import itstep.valva4eva.entity.User;
import itstep.valva4eva.service.UserService;

public abstract class FrontCommand {
	protected ServletContext context;
	protected HttpServletRequest request;
	protected HttpServletResponse response;
	protected DataSource ds;

	// передаются объекты из сервлета
	public void init(ServletContext servletContext, HttpServletRequest servletRequest,
			HttpServletResponse servletResponse, DataSource servletDataSource) {
		this.context = servletContext;
		this.request = servletRequest;
		this.response = servletResponse;
		this.ds = servletDataSource;
	}
	public abstract void process() throws ServletException, IOException;

	protected void forward(String target) throws ServletException, IOException {
		target = String.format("/WEB-INF/jsp/%s.jsp", target);
		RequestDispatcher dispatcher = context.getRequestDispatcher(target);
		dispatcher.forward(request, response);
	}

	protected void redirect(String target) throws IOException {
		target = String.format("%s/%s.html", request.getContextPath(), target);
		response.sendRedirect(target);
	}
	
	protected boolean loginUser() throws SQLException {
		User user = null;
		HttpSession session = request.getSession(false);
		if (session != null) {
			user = (User) request.getSession().getAttribute("reguser");
			if (user != null && !user.getTel().isEmpty()) {
				UserService us = new UserService(ds);
				User findUser = us.findById(user.getId());
				if (findUser != null) {
					return true;
				}
			}
		}
		return false;
	}
	protected boolean loginAdmin() throws SQLException {
		User user = null;
		HttpSession session = request.getSession(false);
		if (session != null) {
			user = (User) request.getSession().getAttribute("reguser");
			if (user != null && !user.getTel().isEmpty()) {
				UserService us = new UserService(ds);
				User findUser = us.findById(user.getId());
				if (findUser != null && findUser.numRol() == 9) {
					return true;
				}
			}
		}
		return false;
	}
	
}
