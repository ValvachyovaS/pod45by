package itstep.valva4eva.entity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Ordrow implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private int order_id;
	private int service_id;
	private String name;
	private double rate;
	private int count;
	
	public Ordrow(int id, String name, double rate, int count) {
		this.id = id;
		this.name = name;
		this.rate = rate;
		this.count = count;
	}
	
	
}

