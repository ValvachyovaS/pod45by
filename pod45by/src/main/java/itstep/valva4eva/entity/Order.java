package itstep.valva4eva.entity;

import java.io.Serializable;
import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Order implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String datime;
	private String adr;
	private String comment;
	private String status;
	private int discount;
	private int user_id;
	private List<Ordrow> ordrows;
	private User user;
	
	public Order(String adr, String comment, int user_id) {
		this.adr = adr;
		this.comment = comment;
		this.user_id = user_id;
	}

	public Order(int id, String datime, String adr, String comment, String status, int discount, int user_id) {
		this.id = id;
		this.datime = datime;
		this.adr = adr;
		this.comment = comment;
		this.status = status;
		this.discount = discount;
		this.user_id = user_id;
	}
	


}

