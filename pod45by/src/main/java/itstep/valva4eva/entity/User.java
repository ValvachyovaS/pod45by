package itstep.valva4eva.entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String datime;
	private String tel;
	private String pas;
	private String nam;
	private String eml;
	private String rol;
	private List<Order> orders;
	
	
	public User(String tel, String pas, String nam, String eml) {
		this.tel = tel;
		this.pas = pas;
		this.nam = nam;
		this.eml = eml;
	}
	
	public int numRol() {
		String rol = this.rol;
		ResourceBundle roleProp = ResourceBundle.getBundle("role");
		Map<String, Integer> mapRole = new HashMap<>();
		for (int i=0; i<10; i++) {
			if (roleProp.containsKey(String.valueOf(i))) {
				mapRole.put(roleProp.getString(String.valueOf(i)), i);
			}
		}
		return mapRole.get(rol);
	}

	public void setRol(int i) {
		ResourceBundle roleProp = ResourceBundle.getBundle("role");
		String role = String.valueOf(i);
		this.rol = roleProp.getObject(role).toString();
	}
}

