package itstep.valva4eva.entity;

import java.io.Serializable;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Ring implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String datime;
	private String tel;
	private String name;

	public Ring(String tel, String name) {
		this.tel = tel;
		this.name = name;
	}

}
