package itstep.valva4eva.entity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Serv implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String name;
	private double rate;
	private int act;

	public Serv(String name, Double rate) {
		this.name = name;
		this.rate = rate;
	}

}
