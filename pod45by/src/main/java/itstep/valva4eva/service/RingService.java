package itstep.valva4eva.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

import itstep.valva4eva.entity.Ring;

public class RingService {
	private DataSource ds;

	public RingService(DataSource ds){
		this.ds = ds;
	}

	public int create(Ring ring) throws SQLException {
		String sql = "INSERT INTO pod45.rings (datime, tel, name) VALUES (?, ?, ?)";
		int status = 0;
		Timestamp d = null;
		try (Connection con = ds.getConnection()) {
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery("SELECT NOW()");
			if (rs.next()) {
				d = rs.getTimestamp(1);
			}
			if (rs != null) rs.close();
			if (st != null) st.close();
			
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, d.toString());
			ps.setString(2, ring.getTel());
			ps.setString(3, ring.getName());
			status = ps.executeUpdate();
			if (ps != null) ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return status;
	}

	public List<Ring> readAll() throws SQLException {
		List<Ring> ringList = new ArrayList<>();
		String sql = "SELECT * FROM pod45.rings ORDER BY datime DESC";
		try (Connection con = ds.getConnection(); 
				Statement st = con.createStatement()) {
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				Ring ring = new Ring();
				ring.setId((rs.getInt("id")));
				ring.setDatime(rs.getString("datime"));
				ring.setTel(rs.getString("tel"));
				ring.setName(rs.getString("name"));
				ringList.add(ring);
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ringList;
	}
	public List<Ring> searchTel(String tel) throws SQLException {
		tel = "%" + tel + "%";
		List<Ring> ringList = new ArrayList<>();
		String sql = "SELECT * FROM pod45.rings WHERE tel LIKE ?";
		try (Connection con = ds.getConnection(); 
				PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, tel);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Ring ring = new Ring();
				ring.setId((rs.getInt("id")));
				ring.setDatime(rs.getString("datime"));
				ring.setTel(rs.getString("tel"));
				ring.setName(rs.getString("name"));
				ringList.add(ring);
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ringList;
	}

	public int delDate(String date) throws SQLException {
		int status = 0;
		String sql = "DELETE FROM pod45.rings WHERE datime<?";
		try (Connection con = ds.getConnection(); 
				PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, date);
			status = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return status;
	}
}