package itstep.valva4eva.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

import itstep.valva4eva.entity.Serv;

public class ServService {
	private DataSource ds;

	public ServService(DataSource ds) {
		this.ds = ds;
	}

	public int create(Serv serv) throws SQLException {
		int increment = 0, status = 0;
		String sql = "INSERT INTO pod45.services (name, rate) VALUES (?, ?)";
		try (Connection con = ds.getConnection(); 
				PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
			ps.setString(1, serv.getName());
			ps.setDouble(2, serv.getRate());
			status = ps.executeUpdate();
			if (status == 1) {
				ResultSet rs = ps.getGeneratedKeys();
				rs.next();
				increment = rs.getInt(1);
				rs.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return increment;
	}

	public List<Serv> readAll() throws SQLException {
		List<Serv> servList = new ArrayList<>();
		String sql = "SELECT * FROM pod45.services";
		try (Connection con = ds.getConnection(); 
				Statement st = con.createStatement()) {
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				Serv serv = new Serv();
				serv.setId((rs.getInt("id")));
				serv.setName(rs.getString("name"));
				serv.setRate(rs.getDouble("rate"));
				serv.setAct(rs.getInt("act"));
				servList.add(serv);
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return servList;
	}

	public List<Serv> readAct(int act) throws SQLException {
		List<Serv> servList = new ArrayList<>();
		String sql = "SELECT * FROM pod45.services WHERE act=?";
		try (Connection con = ds.getConnection(); 
				PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setInt(1, act);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Serv serv = new Serv();
				serv.setId((rs.getInt("id")));
				serv.setName(rs.getString("name"));
				serv.setRate(rs.getDouble("rate"));
				serv.setAct(rs.getInt("act")); // после проверки удалить
				servList.add(serv);
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return servList;
	}

	public Serv findById(int id) throws SQLException {
		String sql = "SELECT * FROM pod45.services WHERE id=?";
		Serv serv = null;
		try (Connection con = ds.getConnection(); 
				PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				serv = new Serv(rs.getInt("id"), rs.getString("name"), rs.getDouble("rate"), 
						rs.getInt("act"));
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return serv;
	}
	
	public int update(Serv serv) throws SQLException {
		int status = 0;
		String sql = "UPDATE pod45.services SET act=? WHERE id=?";
		try (Connection con = ds.getConnection(); 
				PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setInt(1, serv.getAct());
			ps.setInt(2, serv.getId());
			status = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return status;
	}
	
	public int delDeact(int id) throws SQLException {
		int status = 0;
		String sql = "SELECT COUNT(*) FROM pod45.ordrows JOIN pod45.services "
				+ "ON pod45.ordrows.service_id=pod45.services.id WHERE service_id=" + id;
		String sqlDel = "DELETE FROM pod45.services WHERE id=" + id;
		try (Connection con = ds.getConnection()) {
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(sql);
			if (rs.next()) {
				if (rs.getInt(1) == 0) {
					PreparedStatement ps = con.prepareStatement(sqlDel);
					status = ps.executeUpdate();
					if (ps != null) ps.close();
				}
			}
			if (rs != null) rs.close();
			if (st != null) st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return status;
	}
}