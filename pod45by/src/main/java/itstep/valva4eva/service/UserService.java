package itstep.valva4eva.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import itstep.valva4eva.entity.User;

public class UserService {
	private DataSource ds;

	public UserService(DataSource ds) {
		this.ds = ds;
	}

	public int create(User user) throws SQLException {
		int status = 0;
		Timestamp d = null;
		String sql = "INSERT INTO pod45.users (datime, tel, pas, nam, eml) VALUES (?, ?, ?, ?, ?)";
		try (Connection con = ds.getConnection()) {
				Statement st = con.createStatement();
				ResultSet rs = st.executeQuery("SELECT NOW()");
				if (rs.next()) {
					d = rs.getTimestamp(1);
				}
				if (rs != null) rs.close();
				if (st != null) st.close();
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, d.toString());
			ps.setString(2, user.getTel());
			ps.setString(3, user.getPas());
			ps.setString(4, user.getNam());
			ps.setString(5, user.getEml());
			status = ps.executeUpdate();
			if (ps != null) ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return status;
	}

	public List<User> readAll() throws SQLException {
		List<User> userList = new ArrayList<>();
		String sql = "SELECT * FROM pod45.users ORDER BY datime DESC";
		try (Connection con = ds.getConnection(); 
				Statement st = con.createStatement()) {
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				User user = new User();
				user.setId((rs.getInt("id")));
				user.setDatime(rs.getString("datime"));
				user.setTel(rs.getString("tel"));
				user.setPas(rs.getString("pas"));
				user.setNam(rs.getString("nam"));
				user.setEml(rs.getString("eml"));
				user.setRol(rs.getInt("rol"));
				userList.add(user);
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return userList;
	}

	public User findByTel(String tel) throws SQLException {
		String sql = "SELECT * FROM pod45.users WHERE tel=?";
		User user = null;
		try (Connection con = ds.getConnection(); 
				PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, tel);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				user = new User();
				user.setId((rs.getInt("id")));
				user.setDatime(rs.getString("datime"));
				user.setTel(rs.getString("tel"));
				user.setPas(rs.getString("pas"));
				user.setNam(rs.getString("nam"));
				user.setEml(rs.getString("eml"));
				user.setRol(rs.getInt("rol"));
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}

	public User findById(int id) throws SQLException {
		String sql = "SELECT * FROM pod45.users WHERE id=?";
		User user = null;
		try (Connection con = ds.getConnection(); 
				PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				user = new User();
				user.setId((rs.getInt("id")));
				user.setDatime(rs.getString("datime"));
				user.setTel(rs.getString("tel"));
				user.setPas(rs.getString("pas"));
				user.setNam(rs.getString("nam"));
				user.setEml(rs.getString("eml"));
				user.setRol(rs.getInt("rol"));
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}

	public int update(User user) throws SQLException {
		int status = 0;
		String sql = "UPDATE pod45.users SET pas=?, nam=?, eml=?, rol=? WHERE id=?";
		try (Connection con = ds.getConnection(); 
				PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, user.getPas());
			ps.setString(2, user.getNam());
			ps.setString(3, user.getEml());
			ps.setInt(4, user.numRol());
			ps.setInt(5, user.getId());
			status = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return status;
	}
	
	public int delete(User user) throws SQLException {
		int status = 0;
		String sql = "DELETE FROM pod45.users WHERE id=?";
		try (Connection con = ds.getConnection(); 
				PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setInt(1, user.getId());
			status = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return status;
	}
}