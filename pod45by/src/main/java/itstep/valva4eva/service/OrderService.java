package itstep.valva4eva.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.sql.DataSource;

import itstep.valva4eva.entity.Order;
import itstep.valva4eva.entity.Ordrow;

public class OrderService {
	private DataSource ds;

	public OrderService(DataSource ds){
		this.ds = ds;
	}

	public int createRow(Ordrow ord) throws SQLException {
		int status = 0;
		String sql = "INSERT INTO pod45.ordrows (order_id, service_id, count) VALUES (?, ?, ?)";
		try (Connection con = ds.getConnection(); 
				PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setInt(1, ord.getOrder_id());
			ps.setInt(2, ord.getService_id());
			ps.setDouble(3, ord.getCount());
			status = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return status;
	}

	public int create(Order order) throws SQLException {
		Timestamp d = null;
		try (Connection con = ds.getConnection(); 
				Statement st = con.createStatement()) {
			ResultSet rs = st.executeQuery("SELECT NOW()");
			if (rs.next()) {
				d = rs.getTimestamp(1);
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		int increment = 0, status = 0;
		String sql = "INSERT INTO pod45.orders (datime, adr, comment, user_id) VALUES (?, ?, ?, ?)";
		try (Connection con = ds.getConnection(); 
			PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
			ps.setString(1, d.toString());
			ps.setString(2, order.getAdr());
			ps.setString(3, order.getComment());
			ps.setInt(4, order.getUser_id());
			status = ps.executeUpdate();
			if (status == 1) {
				ResultSet rs = ps.getGeneratedKeys();
				rs.next();
				increment = rs.getInt(1);
				rs.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return increment;
	}

	public int update(Order order) throws SQLException {
		int status = 0;
		String sql = "UPDATE pod45.orders SET adr=?, comment=?, status=?, discount=? WHERE id=?;";
		try (Connection con = ds.getConnection(); 
				PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, order.getAdr());
			ps.setString(2, order.getComment());
			ps.setString(3, order.getStatus());
			ps.setInt(4, order.getDiscount());
			ps.setInt(5, order.getId());
			status = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return status;
	}

	public List<Order> readByUserId(int user_id) throws SQLException {
		List<Order> orderList = new ArrayList<>();
		String sql = "SELECT * FROM pod45.orders WHERE user_id=? ORDER BY datime DESC";
		try (Connection con = ds.getConnection(); 
				PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setInt(1, user_id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Order order = new Order();
				order.setId(rs.getInt("id"));
				order.setDatime(rs.getString("datime"));
				order.setAdr(rs.getString("adr"));
				order.setUser_id(rs.getInt("user_id"));
				order.setComment(rs.getString("comment"));
				order.setStatus(rs.getString("status"));
				order.setDiscount(rs.getInt("discount"));
				orderList.add(order);
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return (orderList != null) ? orderList : Collections.emptyList();
	}
	
	public List<Order> readAll() throws SQLException {
		List<Order> orderList = new ArrayList<>();
		String sql = "SELECT * FROM pod45.orders ORDER BY datime DESC";
		try (Connection con = ds.getConnection(); 
				Statement st = con.createStatement()) {
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				Order order = new Order();
				order.setId(rs.getInt("id"));
				order.setDatime(rs.getString("datime"));
				order.setAdr(rs.getString("adr"));
				order.setUser_id(rs.getInt("user_id"));
				order.setComment(rs.getString("comment"));
				order.setStatus(rs.getString("status"));
				order.setDiscount(rs.getInt("discount"));
				orderList.add(order);
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return orderList;
	}
	
	public List<Order> readByStatus(String status) throws SQLException {
		List<Order> orderList = new ArrayList<>();
		String sql = "SELECT * FROM pod45.orders WHERE status=? ORDER BY datime DESC";
		try (Connection con = ds.getConnection(); 
				PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, status);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
			Order order = new Order(rs.getInt("id"), rs.getString("datime"), rs.getString("adr"), rs.getString("comment"),
						rs.getString("status"), rs.getInt("discount"), rs.getInt("user_id"));
			orderList.add(order);
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return orderList;
	}

	public Order findById(int id) throws SQLException {
		String sql = "SELECT * FROM pod45.orders WHERE id=?";
		Order order = null;
		try (Connection con = ds.getConnection(); 
				PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				order = new Order(rs.getInt("id"), rs.getString("datime"), rs.getString("adr"), rs.getString("comment"),
						rs.getString("status"), rs.getInt("discount"), rs.getInt("user_id"));
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return order;
	}

	public List<Ordrow> readAllRows(int id) {
		String sql = "SELECT ordrows.id, services.name, services.rate, ordrows.count "
				+ "FROM orders JOIN ordrows ON orders.id=ordrows.order_id "
				+ "JOIN services ON ordrows.service_id=services.id WHERE orders.id = ?";
		List<Ordrow> ordrows = new ArrayList<>();
		try (Connection con = ds.getConnection(); 
				PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Ordrow ordrow = new Ordrow(rs.getInt("id"), rs.getString("name"), rs.getDouble("rate"),
						rs.getInt("count"));
				ordrows.add(ordrow);
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ordrows;
	}
	
	public int delete(Order order) throws SQLException {
		int status = 0;
		String sqlRow = "DELETE FROM pod45.ordrows WHERE order_id=?";
		String sqlOrd = "DELETE FROM pod45.orders WHERE id=?";
		
		try (Connection con = ds.getConnection()) {
			PreparedStatement ps = con.prepareStatement(sqlRow);
			ps.setInt(1, order.getId());
			status = ps.executeUpdate();
			if(status > 0) {
				ps = con.prepareStatement(sqlOrd);
				ps.setInt(1, order.getId());
				status = ps.executeUpdate();
			}
			if(ps!=null) ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return status;
	}
}