package itstep.valva4eva.tool;

import java.util.List;
import java.util.Properties;
import java.util.ResourceBundle;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import itstep.valva4eva.entity.Order;
import itstep.valva4eva.entity.Ordrow;
import itstep.valva4eva.entity.User;

public class Mailer {
	
	public static void send(String sub, String msg, String eml) throws MessagingException {
		ResourceBundle mailProps = ResourceBundle.getBundle("mail");
		Properties props = new Properties();
		props.put("mail.smtp.host", mailProps.getObject("host").toString());
		props.put("mail.smtp.ssl.trust", mailProps.getObject("host").toString());
		props.put("mail.smtp.port", mailProps.getObject("port").toString());
		props.put("mail.smtp.auth", "true"); // аутентифицировать пользователя с помощью команды AUTH.
		props.put("mail.smtp.starttls.enable", "true");

		Session session = Session.getInstance(props, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(mailProps.getObject("user").toString(),
						mailProps.getObject("password").toString());
			}
		});
		MimeMessage message = new MimeMessage(session);
		message.setFrom(new InternetAddress(mailProps.getObject("user").toString()));
		message.addRecipient(Message.RecipientType.TO, new InternetAddress(eml));
		message.setSubject(sub);
		message.setText(msg);
		Transport.send(message);
		System.out.println("Сообщение на почтовый ящик " + eml + " отправлено...");
	}
	
	public static String textOrder(User user, Order order, List<Ordrow> listOrdrow) {
		double sum = 0;
		StringBuilder sb = new StringBuilder();
		sb.append("Заявка №" + order.getId() + " от " + order.getDatime() + "\n")
			.append("Тел.: +375 " + user.getTel() + " Имя: " + user.getNam() + " E-mail: " + user.getEml() + "\n");
		for (int i=0; i<listOrdrow.size(); i++) {
			sb.append((i+1) +"_"+ listOrdrow.get(i).getName() +"_"+ listOrdrow.get(i).getRate() +"_"+ listOrdrow.get(i).getCount() + "\n");
			sum += listOrdrow.get(i).getRate() * listOrdrow.get(i).getCount();
		}
		sb.append("Адрес: " + order.getAdr() + "\n").append("Комментарий: " + order.getComment() + "\n")
			.append("Статус заявки: " + order.getStatus() + " Скидка: " + order.getDiscount() + "% Сумма(c учетом скидки): "+ (sum + sum*order.getDiscount()/100) + "руб. \n");
		return sb.toString();
	}
	
	public static String textPas(String pas) {
		StringBuilder sb = new StringBuilder();
		sb.append("Поступил запрос на восстановление забытого пароля на сайте pod45.by \n");
		sb.append("Password: " + pas + "\n");
		sb.append("Если вы не делали этого запроса, то просто удалите данное сообщение. Пароль "
				+ "зашифрован и хранится в надежном месте.");
		return sb.toString();
	}
}
