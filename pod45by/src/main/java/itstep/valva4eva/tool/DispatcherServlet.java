package itstep.valva4eva.tool;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.ResourceBundle;

import itstep.valva4eva.command.FrontCommand;
import itstep.valva4eva.command.PageNotFoundCommand;

//Front Controller
public class DispatcherServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Resource(name = "jdbc/pod45")
	private DataSource ds;
	private ResourceBundle mappingProps;
	private Map<String, FrontCommand> commands;
	

	@Override
	public void init() throws ServletException {
		super.init();
		this.mappingProps = ResourceBundle.getBundle("mapping");
		this.commands = new HashMap<>();
		commands.put("404", new PageNotFoundCommand());
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		FrontCommand command = getCommand(request);
		command.init(getServletContext(), request, response, ds);
		command.process();
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		FrontCommand command = getCommand(request);
		command.init(getServletContext(), request, response, ds);
		command.process();
	}

	private FrontCommand getCommand(HttpServletRequest request) {
		String action = getActionKey(request);
		FrontCommand command = commands.get(action);
		if (Objects.isNull(command)) {
			try {
				Class<?> type = Class.forName(mappingProps.getObject(action).toString());
				command = type.asSubclass(FrontCommand.class).getDeclaredConstructor().newInstance();
				commands.put(action, command);
			} catch (Exception e) {
				command = commands.get("404");
			}
		}
		return command;
	}

	private String getActionKey(HttpServletRequest request) {
		String path = request.getServletPath();
		int slash = path.lastIndexOf("/");
		int period = path.lastIndexOf(".");
		if (period >= 0 && period > slash) {
			path = path.substring(slash + 1, period);
		}
		return path;
	}
}
