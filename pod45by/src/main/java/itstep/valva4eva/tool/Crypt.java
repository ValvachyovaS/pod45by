package itstep.valva4eva.tool;

import java.io.IOException;
import java.util.Arrays;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class Crypt {
	private static Crypt instance;
	private SecretKeySpec secretKey;
	private Cipher cipher;

	private Crypt() {
	try {
			this.cipher = Cipher.getInstance("AES");
			String code = "KeramoGranit";
			byte[] key = code.getBytes("UTF-8");
			MessageDigest sha = MessageDigest.getInstance("SHA-1");
			key = sha.digest(key);
			key = Arrays.copyOf(key, 16); // 128bit
			this.secretKey = new SecretKeySpec(key, "AES");
		} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}

	public String encrypt(String aData) {
		String result = "";
		try {
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			byte[] utf8 = aData.getBytes("UTF8");
			byte[] encryptedData = cipher.doFinal(utf8);
			result = Base64.getEncoder().encodeToString(encryptedData);
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	public String decrypt(String aData) {
		String result = "";
		try {
			cipher.init(Cipher.DECRYPT_MODE, secretKey);
			byte[] decodedData = Base64.getDecoder().decode(aData);
			byte[] utf8 = cipher.doFinal(decodedData);
			result = new String(utf8, "UTF8");
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return result;
	}

	public static Crypt getInstance() {
		if (instance == null) {
			instance = new Crypt();
		}
		return instance;
	}
}
