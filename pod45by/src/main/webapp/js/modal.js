 $('#ringform').trigger('reset');
 $(function () {
     'use strict';
     $('#ringform').on('submit', function (e) {
         e.preventDefault();
         $.ajax({
             url: 'send.php',
             type: 'POST',
             contentType: false,
             processData: false,
             data: new FormData(this),
             success: function (msg) {
                 if (msg == 'ok') {
                     bootbox.alert('Сообщение отправлено. Мастер перезвонит в ближайшее рабочее время.');
                     $('#ringform').trigger('reset');
                     $('.bd-example-modal-sm').modal('hide');
                 } else {
                     bootbox.alert('Ошибка! Сообщение не отправлено. Будьте добры, сообщите мастеру об ошибке.');
                 }
             }
         });
     });
 });

 $('#mesform').trigger('reset');
 $(function () {
     'use strict';
     $('#mesform').on('submit', function (e) {
         e.preventDefault();
         $.ajax({
             url: 'sendev.php',
             type: 'POST',
             contentType: false,
             processData: false,
             data: new FormData(this),
             success: function (msg) {
                 if (msg == 'ok') {
                     bootbox.alert('Сообщение отправлено. Благодарю за проявленный интерес к ресурсу.');
                     $('#mesform').trigger('reset');
                     $('.bd-modal-sm').modal('hide');
                 } else {
                     bootbox.alert('Ошибка.');
                 }
             }
         });
     });
 });
