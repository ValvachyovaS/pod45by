<%@ page language="java" contentType="text/html; charset=UTF-8" 
pageEncoding="UTF-8" trimDirectiveWhitespaces="true" isErrorPage="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css"/>"/>
    <title>Error</title>
    <link rel="shortcut icon" type="image/png" href="<c:url value="/img/favicon.png"/>"/>
</head>
<body>
<div class="container">
	<jsp:include page="navbar.jsp"></jsp:include>
    <div class="jumbotron">
        <h1 class="display-4">Ошибка динамической страницы!</h1>
        <p class="lead">Извините, произошла ошибка в отображении jsp-страницы. 
        <br>Пожалуйста, попробуйте позже либо обратитесь к разработчику.</p>
        <p><%=exception %></p>
        <hr class="my-4">
        <a class="btn btn-primary btn-lg" href="<c:url value="/index.html"/>" role="button">На главную</a>
    </div>
    <jsp:include page="footer.jsp"></jsp:include>
</div>
</body>
</html>