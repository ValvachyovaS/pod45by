<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true"
	errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<html lang="ru">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css"/>" />
<link rel="stylesheet" href="<c:url value="/css/styles.css"/>" />
<title>pod45.by Управление заявками</title>
<link rel="shortcut icon" type="image/png" href="<c:url value="/img/favicon.png"/>" />
</head>
<body>
	<div class="container">
		<jsp:include page="navbar.jsp"></jsp:include>
		<div class="container-item mw1000">
			<h3>Заявки пользователей:</h3>
			<p class="lead">Для отображения нужных заявок по статусу выберите и нажмите "Показать": </p>
			<p class="lead msg-error" id="admin-order-msg">${msgAdminOrder}</p>
			<div class="container-item">
			<form method="POST">
			<div class="custom-control custom-switch custom-control-inline">
  <input type="radio" class="custom-control-input" id="customSwitch1" name="radio-status" value="Оформлена"
  ${rs eq 'Оформлена'? 'checked' : ''}>
  <label class="custom-control-label" for="customSwitch1">Оформлены</label>
</div>
<div class="custom-control custom-switch custom-control-inline">
  <input type="radio" class="custom-control-input" id="customSwitch2" name="radio-status" value="Принята"
  ${rs eq 'Принята'? 'checked' : ''}>
  <label class="custom-control-label" for="customSwitch2">Приняты</label>
</div>
<div class="custom-control custom-switch custom-control-inline">
  <input type="radio" class="custom-control-input" id="customSwitch3" name="radio-status" value="Выполнена"
  ${rs eq 'Выполнена'? 'checked' : ''}>
  <label class="custom-control-label" for="customSwitch3">Выполнены</label>
</div>
<div class="custom-control custom-switch custom-control-inline">
  <input type="radio" class="custom-control-input" id="customSwitch4" name="radio-status" value="Отменена"
  ${rs eq 'Отменена'? 'checked' : ''}>
  <label class="custom-control-label" for="customSwitch4">Отменены</label>
</div>
<div class="custom-control custom-switch custom-control-inline">
  <input type="radio" class="custom-control-input" id="customSwitch5" name="radio-status" value="Все"
  ${rs eq 'Все'? 'checked' : ''}>
  <label class="custom-control-label" for="customSwitch5">Все</label>
</div>
			<button type="submit" class="btn btn-primary" id="btn-status">Показать</button>
			</form>
			</div>
			<br>
			<div class="container-item">
			<p class="lead">Поиск по телефону: </p>
		<form method="POST" action="" id="form-search-order">
					<div class="input-group">
						<span class="input-group-text">+375 (</span> 
						<input type="text" name="tel1" id="admin-order-tel1" maxlength="2" required pattern="[0-9]{2,}" 
						title="Код мобильного оператора: 25 29 33 44" size="2"> 
						<span class="input-group-text">) </span> 
						<input class="icons" type="text" name="tel2" id="admin-order-tel2" maxlength="7" required
							pattern="[0-9]{7,}" title="Телефон в формате +375 (хх) ххххххх" size="7">
					<button type="submit" class="btn btn-success">Найти</button>
					</div>
		</form>
			</div>
				<br>
			<c:if test="${empty listOrder}">
				<p class="lead">Ничего не найдено. Список заявок пуст.</p>
			</c:if>
			<c:if test="${not empty listOrder}">
				<table class="table table-info table-sm" id="table-admin-order">
					<c:forEach items="${listOrder}" var="order">
						<tr>
							<td class="font-weight-bold">№ ${order.id} от <br>${fn:substring(order.datime, 0, 16)}</td>
							<td>Тел: +375 ${order.user.getTel()} <br>Имя: ${order.user.getNam()}</td>
							<td rowspan="2" style="width: 300px" class="small text-muted">Комментарий: ${order.comment}</td>
						</tr>
						<tr>
							<td colspan="2">Адрес: ${order.adr}</td>
						</tr>
						<tr>
							<td colspan="2">
								<table class="table table-bordered table-secondary table-striped table-sm">
									<c:set var="sumOrder" value="${0}" />
									<c:forEach items="${order.ordrows}" var="ordrow"
										varStatus="status">
										<tr>
											<td>${status.count}</td>
											<td>${ordrow.name}</td>
											<td>${ordrow.rate}</td>
											<td>${ordrow.count}</td>
											<td><c:out value="${ordrow.rate*ordrow.count}" /></td>
										</tr>
										<c:set var="sumOrder"
											value="${sumOrder+ordrow.rate*ordrow.count}" />
									</c:forEach>
								</table>
							</td>
							<td>
								<form>
								<input type="hidden" name="order-id-status" value="${order.id}">
									<div class="input-group mb-1">
										<span class="input-group-text">Статус</span> 
										<select class="custom-select" name="status">
											<option value="Оформлена" ${order.status eq 'Оформлена'? 'selected' : ''}>Оформлена</option>
											<option value="Принята" ${order.status eq 'Принята'? 'selected' : ''}>Принята</option>
											<option value="Выполнена" ${order.status eq 'Выполнена'? 'selected' : ''}>Выполнена</option>
											<option value="Отменена" ${order.status eq 'Отменена'? 'selected' : ''}>Отменена</option>
										</select>
										<div class="input-group-append">
											<button type="submit" class="btn btn-outline-secondary"
												type="button">Установить</button>
										</div>
									</div>
								</form>
								<form>
								<input type="hidden" name="order-id-discount" value="${order.id}">
									<div class="input-group mb-1">
										<span class="input-group-text">Скидка, %</span> <input
											type="number" class="form-control" required name="discount"
											min='0' max='100' step='5' value="${order.discount}">
										<div class="input-group-append">
											<button type="submit"
												class="btn btn-outline-secondary d-inline" type="button"
												id="button-addon2">Изменить</button>
										</div>
									</div>
								</form>
							</td>
						</tr>
						<tr class="bg-dark text-white">
							<td colspan="3" class="text-right">Сумма: ${sumOrder} руб.
								Сумма с учетом скидки: <span class="h3">
								 <fmt:formatNumber type="number" groupingUsed="false" maxFractionDigits = "1" 
								 value="${sumOrder-(sumOrder*order.discount/100)}"/></span>руб.
							</td>
						</tr>
						<c:set var="sumOrder" value="0" />
					</c:forEach>
					</tbody>
				</table>
			</c:if>
			<br>
		</div>
		<jsp:include page="footer.jsp"></jsp:include>
	</div>
	<script type="text/javascript" src="<c:url value="/js/jquery.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/js/bootstrap.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/js/bootbox.min.js"/>"></script>
</body>
</html>