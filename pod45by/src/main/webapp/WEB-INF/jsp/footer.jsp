<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true"
	errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<style type="text/css">
.foot-box {
    display: flex;
    flex-flow: row nowrap;
    justify-content: space-around;
    align-items: flex-start;
    padding: 5px 0 20px 0;
    background: url(<c:url value="img/bg-stone.jpg"/>) repeat;
}
@media screen and (max-width: 995px) {

    .foot-box {
        flex-flow: column nowrap;
    }
}
</style>

<div class="bg-warning p-1"></div>
<div class="foot-box bg-grey text-white">
	<div class="mw300 pl-4">
		<div class="h3"><img class="mr-1" src="img/icon-saw.png">pod45.by</div>
		<p>2019 &copy; ИП Шамшуро А.Н.<br>
		Свид. №391389761<br>
		Разработчик: ValvachyovaS</p>
	</div>

	<div class="mw300 pl-4">
		<ul class="nav flex-sm-column">
			<li class="nav-item font-weight-bolder">Услуги:</li>
			<li class="nav-item"><a class="text-reset"
				href="index.html#rezka"> &bull; Прямой рез</a></li>
			<li class="nav-item"><a class="text-reset"
				href="index.html#pod45"> &bull; Фрезеровка под 45<sup>о</sup></a></li>
			<li class="nav-item"><a class="text-reset"
				href="index.html#stup"> &bull; Ступени</a></li>
			<li class="nav-item"><a class="text-reset"
				href="index.html#modul"> &bull; Модули наружного угла</a></li>
			<li class="nav-item"><a class="text-reset"
				href="index.html#stol"> &bull; Столешницы</a></li>
			<li class="nav-item"><a class="text-reset" href="index.html#otv">
					&bull; Отверстия</a></li>
		</ul>
	</div>
	<div class="mw300 pl-4">
		<div class="font-weight-bolder">Контакты:</div>
		<p>Телефон: +375 (29) 217-67-15 (мтс, viber)<br>
		<div class="m-1">
			<a class="btn btn-outline-light border btn-sm" href="<c:url value="/ring.html"/>"
		role="button">Заказать звонок</a>
		</div>
		<div>Адрес мастерской: г.Витебск, ул.Гагарина, д.143 (ГПК-18)</div>
		<div>Время работы: пн-пт 9:00-17:00</div>
	</div>
</div>

<script type="text/javascript">
</script>
