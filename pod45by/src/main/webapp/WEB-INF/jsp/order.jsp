<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true"
	errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html lang="ru">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css"/>" />
<link rel="stylesheet" href="<c:url value="/css/styles.css"/>" />
<title>pod45.by - Оформление заявки</title>
<link rel="shortcut icon" type="image/png" href="<c:url value="/img/favicon.png"/>"/>
</head>
<body>
	<div class="container">
	<jsp:include page="navbar.jsp"></jsp:include>
	<div class="container-item">
		<h4>Оформление заявки на обработку плитки:</h4>
		<p class="lead msg-error" id="order-next-msg">${msgOrder}</p>
		<c:if test="${empty sessionScope.listService}">
			<a class="btn btn-primary" href="<c:url value="/index.html"/>" role="button">На главную</a>
			<br><br>
		</c:if>
		<c:if test="${not empty sessionScope.listService}">
		<div class="d-flex flex-row flex-wrap justify-content-around">
			<div style="max-width: 400px">
				<p class="lead">Добавьте услуги <br>в необходимом количестве:</p>
				<table class="table table-dark table-sm" id="table_service">
					<thead><tr>
							<th scope="col">Наименование услуги</th>
							<th scope="col">Ставка</th>
							<th scope="col">Добавить</th>
					</tr></thead>
					<tbody>
						<c:forEach items="${listService}" var="serv" varStatus="status">
							<tr>
								<td>${serv.name}</td>
								<td>${serv.rate}</td>
								<td><form method="POST">
										<div class="d-flex flex-row flex-nowrap">
											<input type="hidden" name="service-id" value="${serv.id}">
											<input style="width: 50px" type="number" required id="count"
												name="count" min="1" step="1"> <input type="submit"
												value="+">
										</div>
									</form></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<div style="max-width: 400px">
			<br>
				<p class="lead">Состав заявки:</p>
				<table class="table table-info table-sm" id="table_order">
					<thead>
						<tr>
							<th scope="col">#</th>
							<th scope="col">Id</th>
							<th scope="col">Наименование услуги</th>
							<th scope="col">Цена</th>
							<th scope="col">Кол-во</th>
							<th scope="col">Ставка</th>
							<th scope="col">Удалить</th>
						</tr>
					</thead>
					<tbody>
						<c:set var="sumOrder" value="${0}" />
						<c:forEach items="${listOrdrow}" var="ordrow" varStatus="status">
							<tr>
								<td>${status.count}</td>
								<td>${ordrow.id}</td>
								<td>${ordrow.name}</td>
								<td>${ordrow.rate}</td>
								<td>${ordrow.count}</td>
								<td><c:out value="${ordrow.rate*ordrow.count}" /></td>
								<td>
									<form method="POST">
										<input type="hidden" name="ordrow-id" value="${ordrow.id}">
										<input type="submit" value="-">
									</form>
								</td>
							</tr>
							<c:set var="sumOrder"
								value="${sumOrder+ordrow.rate*ordrow.count}" />
						</c:forEach>
					</tbody>
				</table>
				<p class="lead">Сумма заявки: <b>${sumOrder} руб.</b></p>
				<form>
					<input type="hidden" name="clear" value="clear">
					<button type="submit" class="btn btn-warning" 
					title="Удалить все услуги в заявке">Очистить</button>
				</form>
				<br>
				<p>Заполните адрес и комментарий, если это необходимо:</p>
				<form method="POST" action="" id="orderforma" onsubmit="sendOrder()">
					<table>
						<tr>
							<td><label>Телефон:</label></td>
							<td>+375 <input type="text" name="tel2" id="tel2" readonly
								class="icons bg-grey text-white" value="${sessionScope.reguser.getTel()}"></td>
						</tr>
						<tr>
							<td><label>Имя:</label></td>
							<td><input type="text" name="nam" id="nam" readonly
								class="icons bg-grey text-white" value="${sessionScope.reguser.getNam()}"></td>
						</tr>
						<tr>
							<td><label for="adr">Адрес:</label></td>
							<td><input type="text" name="adr" id="adr" maxlength="50"
								class="icons" size="25" value="${sessionScope.neworder.getAdr()}"
								title="Если необходима доставка, введите адрес. Доставка платная, оговаривается отдельно с мастером">
							</td>
								
						</tr>
						<tr>
							<td><label for="comment">Комментарий:</label></td>
							<td><input type="text" name="comment" id="comment"
								maxlength="200" class="icons" size="35" 
								value="${sessionScope.neworder.getComment()}" 
								title="Если имя и телефон, указанные при регистрации, отличаются от причастных к заявке-укажите тут иные имя и телефон.">
							</td></tr>
					</table>
					<br>
					<input type="hidden" name="orderforma" value="submit">
					<c:if test="${sumOrder > 0}">
					<button type="submit" class="btn btn-success">Отправить	заявку</button>
					</c:if>
					<c:if test="${sumOrder == 0}">
					<button type="submit" class="btn btn-success" 
					disabled title="Необходимо добавить услуги в заявку">Отправить заявку</button>
					</c:if>
					<a class="btn btn-primary" href="<c:url value="/index.html"/>" role="button">На главную</a>
					<br><br>
				</form>
			</div>
		</div>
		</c:if>
	</div>
	<jsp:include page="footer.jsp"></jsp:include>
	</div>
	<script type="text/javascript" src="<c:url value="/js/jquery.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/js/bootstrap.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/js/bootbox.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/js/scripts.js"/>"></script>	
	<script>
	function sendOrder() {
		document.getElementById('order-next-msg').innerHTML = "Заявка отправляется... Подождите, пожалуйста.";
	}
	</script>
</body>
</html>