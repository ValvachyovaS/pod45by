<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html lang="ru">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css"/>" />
<link rel="stylesheet" href="<c:url value="/css/styles.css"/>" />
<title>pod45.by Управление</title>
<link rel="shortcut icon" type="image/png" href="<c:url value="/img/favicon.png"/>" />
</head>
<body>
	<div class="container">
		<jsp:include page="navbar.jsp"></jsp:include>
		<div class="container-item mw1000">
			<h3>Администрирование системы:</h3>
			<br><a class="btn btn-light" href="<c:url value="/index.html"/>" role="button">На главную</a> <br>
			<br><div class="card-group">
				<div class="card text-white bg-secondary mb-3">
					<div class="card-body">
						<h5 class="card-title"><img src="<c:url value="/img/icon-list.png"/>" alt="">Заявки</h5>
						<p class="card-text">Просмотр заявок<br>Поиск по телефону пользователя<br>
							Установка статуса и скидки</p>
					</div>
					<div class="card-footer">
						<a class="btn btn-dark" href="<c:url value="/admin-order.html"/>" role="button">Перейти</a>
					</div>
				</div>
				<div class="card text-white bg-secondary mb-3">
					<div class="card-body">
						<h5 class="card-title"><img src="<c:url value="/img/icon-users.png"/>" alt="">Пользователи</h5>
						<p class="card-text">Смена ролей<br>Просмотр всех пользователей<br> Поиск по
							телефону<br>Каскадное удаление</p>
					</div>
					<div class="card-footer">
						<a class="btn btn-dark" href="<c:url value="/admin-user.html"/>" role="button">Перейти</a>
					</div>
				</div>
				<div class="card text-white bg-secondary mb-3">
					<div class="card-body">
						<h5 class="card-title"><img src="<c:url value="/img/icon-alarm.png"/>" alt="">Звонки</h5>
						<p class="card-text">Просмотр списка звонков<br>Поиск по телефону<br>
							Очистка до указанной даты</p>
					</div>
					<div class="card-footer">
						<a class="btn btn-dark" href="<c:url value="/admin-ring.html"/>" role="button">Перейти</a>
					</div>
				</div>
				<div class="card text-white bg-secondary mb-3">
					<div class="card-body">
						<h5 class="card-title"><img src="<c:url value="/img/icon-tools.png"/>" alt="">Услуги</h5>
						<p class="card-text">Добавление новой услуги<br>Установка активности
						<br>Деактивация<br>Удаление</p>
					</div>
					<div class="card-footer">
						<a class="btn btn-dark"
							href="<c:url value="/admin-service.html"/>" role="button">Перейти</a>
					</div>
				</div>
			</div>
		</div>
		<jsp:include page="footer.jsp"></jsp:include>
	</div>
	<script type="text/javascript" src="<c:url value="/js/jquery.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/js/bootstrap.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/js/bootbox.min.js"/>"></script>
</body>
</html>