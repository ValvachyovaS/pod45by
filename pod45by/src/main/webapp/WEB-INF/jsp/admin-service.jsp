<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true"
	errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE HTML>
<html lang="ru">
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css"/>" />
<link rel="stylesheet" href="<c:url value="/css/styles.css"/>" />
<title>pod45.by Уcлуги</title>
<link rel="shortcut icon" type="image/png" href="<c:url value="/img/favicon.png"/>" />
</head>
<body>
	<div class="container">
		<jsp:include page="navbar.jsp"></jsp:include>
		<br>
		<p class="lead msg-error" id="admin-service-msg">${msgAdminServ}</p>
		<div class="d-flex flex-row flex-wrap justify-content-around">
		<div class="container-item mw500">
			<h3>Список активных услуг:</h3>
			<table class="table table-info table-striped table-sm" id="table-act-service">
				<thead>
					<tr>
						<th scope="col">Id</th>
						<th scope="col">Наименование услуги</th>
						<th scope="col">Цена</th>
						<th scope="col"></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${listActServ}" var="act">
					<tr>
						<td>${act.getId()}</td>
						<td>${act.getName()}</td>
						<td>${act.getRate()}</td>
						<td><div class="custom-control custom-switch custom-control-inline">
						<input type="radio" class="custom-control-input" id="switch${act.getId()}" 
							form="serv-act-forma" name="serv-id-a" value="${act.getId()}">
						<label class="custom-control-label" for="switch${act.getId()}"> </label>
						</div></td>
					</tr>
					</c:forEach>
				</tbody>
			</table>
			<p class="lead">Отметьте услугу и выберите нужное действие:
			</p>
			<form method="POST" id="serv-act-forma">
				<button type="submit" class="btn btn-primary" name="serv-act-btn"
					value="copy">Копировать</button>
				<button type="submit" class="btn btn-secondary" name="serv-act-btn"
					value="deact">Деактивировать</button>
			</form>
		</div>
		<div class="container-item mw500">
			<br>
			<h3>Создание новой услуги:</h3>
			<form method="POST" action="" id="serv-add-forma">
				<div class="input-group">
					<span class="input-group-text">Наименование</span> 
					<input type="text" class="icons" name="servname" id="serv-act-name" maxlength="100" required
						title="Наименование услуги, в скобках - единица измерения" 
						value="${servname}" size="50">
				</div> 
				<br>
				<div class="input-group">
						<span class="input-group-text">Цена, руб.</span> 
						<input type="number" name="servrate" class="icons" id="serv-act-rate"
						required min='0.10' step='0.10' 
						value="${servrate}"
						title="Стоимость услуги за единицу измерения, руб.коп.">
				</div> 
				<br>
					<button type="submit" class="btn btn-success">Добавить</button>
					<button type="reset" class="btn btn-secondary">Очистить</button>
			</form>
			<br>
			<p class="lead">Активные услуги доступны при оформлении заявки.</p>
			<p class="lead">Можно скопировать в поля создания активированную услугу<br> и отредактировать до добавления.</p>
			<p class="lead">Деактивированная услуга удаляется при условии <br>отсутствия внешних связей с заявками.</p>
		</div>
	</div>
		<div class="container-item mw500">	
		<h3>Список деактивированных услуг:</h3>
			<table class="table table-dark table-striped table-sm" id="table-act-service">
				<thead>
					<tr>
						<th scope="col">Id</th>
						<th scope="col">Наименование услуги</th>
						<th scope="col">Цена</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${listDeactServ}" var="deact">
						<tr>
							<td>${deact.getId()}</td>
							<td>${deact.getName()}</td>
							<td>${deact.getRate()}</td>
							<td><div class="custom-control custom-switch custom-control-inline">
							<input type="radio" class="custom-control-input" id="switch${deact.getId()}" 
								form="serv-deact-forma" name="serv-id-d" value="${deact.getId()}">
							<label class="custom-control-label" for="switch${deact.getId()}"> </label>
							</div></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<form method="POST" id="serv-deact-forma">
				<button type="submit" class="btn btn-primary" name="serv-deact-btn"
					value="activ">Активировать</button>
				<button type="submit" class="btn btn-warning" name="serv-deact-btn"
					value="clear">Удалить</button>
			</form>
			<br>
		</div>
		<jsp:include page="footer.jsp"></jsp:include>
	</div>
	<script type="text/javascript" src="<c:url value="/js/jquery.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/js/bootstrap.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/js/bootbox.min.js"/>"></script>
</body>
</html>