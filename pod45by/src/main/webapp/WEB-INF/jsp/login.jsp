<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true"
	errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE HTML>
<html lang="ru">
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css"/>" />
<link rel="stylesheet" href="<c:url value="/css/styles.css"/>" />
<title>pod45.by Аутентификация</title>
<link rel="shortcut icon" type="image/png" href="<c:url value="/img/favicon.png"/>"/>
</head>
<body>
	<div class="container">
		<jsp:include page="navbar.jsp"></jsp:include>
		<div class="container-item mw500">
			<h3>Аутентификация пользователя:</h3>
			<p class="lead">Для оформления заявки необходимо<br>авторизоваться/зарегистрироваться в системе.</p>
			<form method="POST" id="login-form">
				<div class="form-group">
					<label>Телефон:</label>
					<div class="input-group">
						<span class="input-group-text">+375 (</span> 
						<input type="text" name="tel1" id="log-tel1" maxlength="2" required pattern="[0-9]{2,}" 
						title="Код мобильного оператора: 25 29 33 44" size="2" value="${fn:escapeXml(param.tel1)}"> 
						<span class="input-group-text">) </span> 
						<input class="icons" type="text" name="tel2" id="log-tel2" maxlength="7" required
							pattern="[0-9]{7,}" title="Телефон в формате +375 (хх) ххххххх" size="7" value="${fn:escapeXml(param.tel2)}">
					</div>
					<label>Пароль:</label>
					<div class="input-group">
						<input type="password" name="pas" id="log-pas" maxlength="9" autocomplete="off"
							required pattern="[0-9a-zA-Z]{5,9}" title="Латинские буквы и цифры от 5 до 9 символов" 
							class="icons" size="9" value="${fn:escapeXml(param.pas)}">
					</div>
					<p class="lead" style="color: red" id="logmsg">${errLog}</p>
					<input type="submit" class="btn btn-success" value="Войти">
					<a class="btn btn-primary" href="<c:url value="/index.html"/>"role="button">На главную</a> 
					<br><a href="<c:url value="/register.html"/>" title="Нажмите для перехода к процедуре регистрации">Регистрация</a>
					<br><a href="<c:url value="/losepas.html"/>" title="Не переживайте - можно восстановить пароль">Забыли пароль?</a>
				</div>
			</form>
			<br>
		</div>
		<jsp:include page="footer.jsp"></jsp:include>
	</div>
	<script>
		document.getElementById('log-tel1').addEventListener('focus',
				function() {
					document.getElementById('logmsg').innerHTML = "";
				});
		document.getElementById('log-tel2').addEventListener('focus',
				function() {
					document.getElementById('logmsg').innerHTML = "";
				});
		document.getElementById('log-pas').addEventListener('focus',
				function() {
					document.getElementById('logmsg').innerHTML = "";
				});
	</script>
</body>
</html>