<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<html lang="ru">
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css"/>" />
<link rel="stylesheet" href="<c:url value="/css/styles.css"/>" />
<title>pod45.by Профиль</title>
<link rel="shortcut icon" type="image/png" href="<c:url value="/img/favicon.png"/>"/>
</head>
<body>
	<div class="container">
	<jsp:include page="navbar.jsp"></jsp:include>
	<div class="container-item mw500">
		<h3>Профиль пользователя:</h3>
		<p class="lead">При необходимости измените регистрационные данные и нажмите "Сохранить":</p>
	 	<form method="POST" action="" id="accountforma">
			<table>
			<tr><td><label>Телефон:</label></td>
			<td>+375 <input type="text" name="tel2" id="tel2" readonly class="icons bg-grey text-white" 
				value="${sessionScope.reguser.getTel()}"></td></tr>
			<tr><td><label for="pas">Новый пароль:</label></td> 
			<td><input type="password" name="pas" id="pas" maxlength="9" pattern="[0-9a-zA-Z]{5,9}" autocomplete="off"
				title="Латинские буквы и цифры от 5 до 9 символов" class="icons" size="9"></td></tr>
			<tr><td><label for="pas2">Повторите новый пароль:</label></td> 
			<td><input type="password" name="pas2" id="pas2" 
				maxlength="9" pattern="[0-9a-zA-Z]{5,9}" title="Пароли должны совпадать" 
				class="icons" size="9" autocomplete="off"></td></tr>
			<tr><td colspan="2"><p class="lead" style="color:red" id="errPas">${errPassword}</p></td></tr>
			<tr><td><label for="nam">Имя:</label></td> <td> <input type="text" name="nam" id="nam" maxlength="20"
				pattern="[a-zA-Zа-яА-Я\s.-]{2,20}" title="Введите имя 2-20 букв" required
				class="icons" size="20" value="${sessionScope.reguser.getNam()}"></td></tr>
			<tr><td><label for="eml">Электронный адрес:</label></td>
			<td><input type="email" name="eml" id="eml" maxlength="40" class="icons" size="30" 
				title="Ящик нужен для доп.средства связи и восстановления пароля" required 
				value="${sessionScope.reguser.getEml()}"></td></tr>
			</table>
			<br>
			<p class="lead" style="color:red" id="accmsg">${msgAccount}</p>
			<input type="submit" class="btn btn-success" value="Сохранить изменения">
			<a class="btn btn-primary" href="<c:url value="/index.html"/>"
				role="button">На главную</a>
		</form>
		</div>
		<div class="container-item mw1000">
		<h3>История заказов:</h3>
		<c:if test="${empty listOrder}">
		<p class="lead"> Вы пока не оформили ни одной заявки.</p>
		<a class="btn btn-dark" href="<c:url value="/order.html"/>"
			role="button" title="Перейти к оформлению заявки на обработку плитки">Оформить заявку</a> 
		</c:if>
		<c:if test="${not empty listOrder}">
			<table class="table table-hover table-dark table-sm" id="table_order">
			<thead>
				<tr>
					<th scope="col">№ дата, время</th>
					<th scope="col">Статус</th>
					<th scope="col">Адрес доставки</th>
					<th scope="col">Комментарий</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${listOrder}" var="order">
					<tr>
						<td>№ ${order.id} от ${fn:substring(order.datime, 0, 16)}</td>
						<td>${order.status}</td>
						<td>${order.adr}</td>
						<td rowspan="2">${order.comment}</td>
					</tr>
					<tr><td colspan="3">
							<table>
							<c:set var="sumOrder" value="${0}"/>
							<c:forEach items="${order.ordrows}" var="ordrow" varStatus="status">
							<tr>
								<td>${status.count}</td>
								<td>${ordrow.name}</td>
								<td>${ordrow.rate}</td>
								<td>${ordrow.count}</td>
								<td><c:out value="${ordrow.rate*ordrow.count}"/></td>
							</tr>
							<c:set var="sumOrder" value="${sumOrder+ordrow.rate*ordrow.count}"/>
							</c:forEach>
							</table>
					</tr>
					<tr class="bg-primary">
							<td colspan="3" class="text-right">Сумма: ${sumOrder} руб. Скидка: ${order.discount}% 
								Сумма с учетом скидки: <span class="h3">
								 <fmt:formatNumber type="number" groupingUsed="false" maxFractionDigits = "1"
								 value="${sumOrder-(sumOrder*order.discount/100)}"/></span>руб.
							</td>
							<td></td>
				</tr>
				</c:forEach>
			</tbody>
		</table>
		</c:if>
	</div>
	<jsp:include page="footer.jsp"></jsp:include>
</div>
	<script type="text/javascript" src="<c:url value="/js/jquery.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/js/bootstrap.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/js/bootbox.min.js"/>"></script>
</body>
</html>