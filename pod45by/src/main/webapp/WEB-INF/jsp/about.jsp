<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true"
	errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html lang="ru">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="author" content="ValvachyovaS">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/styles.css">
<style>
</style>
<title>pod45.by Мастерская в Витебске</title>
<link rel="shortcut icon" type="image/png" href="<c:url value="/img/favicon.png"/>" />
</head>
<body>
	<a class="top bg-dark" href="#top" role="button" title="Наверх"><img
		src="img/icon-up.png"> </a>
	<div class="container">
		<a id="top"></a>
		<jsp:include page="navbar.jsp"></jsp:include>
		<div class="flex-box">
			<div class="mw50 bg-grey p-4">
				<p>
					Мастерская уже 2 года оказывает свои услуги по резке плитки любой
					сложности. <br>Обрабатываем керамическую плитку, керамогранит,
					натуральный камень без сколов и повреждений по размерам заказчика.
					<br>Помимо того, готовы выполнить работу по изготовлению
					ступеней, подоконников, столешниц, умывальников, панно и многое
					другое.
				</p>
			</div>
			<div class="mw50">
				<img src="img/photo/img_workshop01.jpg"
					class="d-block w-100 rounded-lg border border-dark" alt="Станок">
			</div>
		</div>

		<div class="flex-box">
			<div class="mw50">
				<img src="img/photo/img_workshop02.jpg"
					class="d-block w-100 rounded-lg border border-dark"
					alt="Фото мастерской">
			</div>
			<div class="mw50 bg-yellow p-4">
				<p>
					В своей работе используем профессиональный инструмент, высокоточное
					оборудование, которое позволяет гарантировать 100% результат.
					Выполняем работу быстро, качественно и в срок. <br>У нас
					всегда доступные цены, квалифицированный персонал, высокий сервис
					обслуживания, индивидуальный подход. Наши партнеры по продаже
					материала: <a href="https://gorodvitebsk.by/firms/vsya-plitka"
						target="_blank">Вся плитка</a>, <a href="https://keramika.by/"
						target="_blank">Modus Ceramica</a>, <a
						href="https://www.altagamma.by/shop/vitebsk/" target="_blank">Альтагамма</a>.
				</p>
			</div>
		</div>
		<div class="flex-box">

			<div class="card bg-dark mw25 p-2">
				<img src="img/photo/img_workshop03.jpg" class="card-img-top"
					alt="В процессе работы">
				<div class="card-body">
					<h5 class="card-title text-yellow">В процессе работы</h5>
					<p class="card-text text-white">В результате нашей работы
						получаются нужные детали, изготовленные точно по размерам, в
						чистом виде и упакованные для транспортировки.</p>
				</div>
			</div>

			<div class="mw50 bg-light p-4 m-3 rounded-lg border shadow">
				<p class="h5">Контакты:</p>
				<p>
					Телефон: +375 (29) <b>217-67-15</b> (мтс, viber)
				</p>
				<p>
					Адрес мастерской: г.Витебск, <b>ул.Гагарина, д.143 (ГПК-18)</b>
				</p>
				<p>
					Время работы: <b>пн-пт 9:00-17:00</b>
				</p>
				<img src="img/photo/img_workshop_map.jpg"
					class="d-block w-100 rounded-lg border border-dark"
					alt="Карта проезда к мастерской">
			</div>

			<div class="card bg-dark mw25 p-2">
				<img src="img/photo/img_svet.jpg" class="card-img-top"
					alt="Светильники">
				<div class="card-body">
					<h5 class="card-title text-yellow">
						Светильники <span class="badge badge-success">creative</span>
					</h5>
					<p class="card-text text-white">Точечные светильники,
						встраиваемые в модули из плитки, смотрятся оригинально и практичны
						в использовании. Воплотим любые Ваши идеи!</p>
				</div>
			</div>
		</div>
		<jsp:include page="footer.jsp"></jsp:include>
	</div>
	<script type="text/javascript" src="<c:url value="/js/jquery.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/js/bootstrap.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/js/bootbox.min.js"/>"></script>
</body>
</html>
