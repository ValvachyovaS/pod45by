<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE HTML>
<html lang="ru">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css"/>" />
<link rel="stylesheet" href="<c:url value="/css/styles.css"/>" />
<title>Управление звонками</title>
<link rel="shortcut icon" type="image/png" href="<c:url value="/img/favicon.png"/>"/>
</head>
<body>
	<div class="container">
	<jsp:include page="navbar.jsp"></jsp:include>
	<div class="container-item mw500">
	<h3>Поиск:</h3>
	<p class="lead">Введите телефон: </p>
		<form method="POST" action="" id="form-search-ring">
					<div class="input-group">
					<span class="input-group-text">+375 (</span> <input type="text"
						name="tel1" id="admin-ring-tel1" maxlength="2"
						pattern="[0-9]{2,}" title="Код мобильного оператора: 25 29 33 44"
						size="2"> <span class="input-group-text">) </span> <input
						class="icons" type="text" name="tel2" id="admin-ring-tel2"
						maxlength="7" pattern="[0-9]{1,7}"
						title="Телефон в формате +375 (хх) ххххххх" size="7">
					<button type="submit" class="btn btn-success">Найти</button>
					<a class="btn btn-dark" href="<c:url value="/admin-ring.html"/>"
						role="button">Показать все</a>
				</div>
		</form><br>
		<br>
		<h3>Список звонков:</h3>
		<c:if test="${empty listRings}">
		<p class="lead">Записи звонков не найдены.</p> 
		</c:if>
		<c:if test="${not empty listRings}">
		<table class="table table-hover table-dark table-sm" id="table_ring">
			<thead>
				<tr>
					<th scope="col">#</th>
					<th scope="col">Id</th>
					<th scope="col">Дата_Время</th>
					<th scope="col">Телефон</th>
					<th scope="col">Имя</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${listRings}" var="ring" varStatus="status">
					<tr>
						<td>${status.count}</td>
						<td>${ring.id}</td>
						<td>${fn:substring(ring.datime, 0, 16)}</td>
						<td>${ring.tel}</td>
						<td>${ring.name}</td>
					</tr>
				</c:forEach></tbody></table></c:if><br>
		<br>
		<h3>Очистка:</h3>
	<p class="lead">Введите дату, ранее которой необходимо удалить записи. </p>
		<form method="POST" action="" id="form-clear-ring">
			<label for="dateclear">Дата: </label> <input type="date" name="dateclear" required>	
			<button type="submit" class="btn btn-warning">Удалить</button>
		</form><br>
	</div>
	<jsp:include page="footer.jsp"></jsp:include>
	</div>
	<script type="text/javascript" src="<c:url value="/js/jquery.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/js/bootstrap.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/js/bootbox.min.js"/>"></script>
	<script>
	var formClearRing = document.getElementById("form-clear-ring");
	formClearRing.addEventListener("submit", function(e) {
        var currentForm = this;
        e.preventDefault();
        bootbox.confirm({
		    message: "Удалить записи о звонках?",
		    buttons: {
		        confirm: {
		            label: 'Да',
		            className: 'btn-success'},
		        cancel: {
		            label: 'Нет',
		            className: 'btn-light'}},
		    callback: function (result) {
		        if (result){
                currentForm.submit();
            	}
		    }
        });
    });
	</script>	
</body>
</html>