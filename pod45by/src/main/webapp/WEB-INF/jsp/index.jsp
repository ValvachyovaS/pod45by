<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true"
	errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="ValvachyovaS">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/styles.css">
	<style>
	</style>
	<title>pod45.by Резка плитки в Витебске</title>
	<link rel="shortcut icon" type="image/png" href="<c:url value="/img/favicon.png"/>"/>
</head>
<body>
	<a class="top bg-dark" href="#top" role="button" title="Наверх"><img src="img/icon-up.png"> </a>
	<div class="container">
	<a id="top"></a>
		<jsp:include page="navbar.jsp"></jsp:include>
		 <div class="flex-box">
                <div id="carouselPhoto" class="carousel slide mw50" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselPhoto" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselPhoto" data-slide-to="1"></li>
                        <li data-target="#carouselPhoto" data-slide-to="2"></li>
                        <li data-target="#carouselPhoto" data-slide-to="3"></li>
                        <li data-target="#carouselPhoto" data-slide-to="4"></li>
                        <li data-target="#carouselPhoto" data-slide-to="5"></li>
                        <li data-target="#carouselPhoto" data-slide-to="6"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="img/photo/img07.jpg" class="d-block w-100 rounded-lg border border-dark" alt="Ступени">
                            <div class="carousel-caption d-none d-md-block text-dark">
                                <h5>Ступени</h5>
                                <p>с капиносами и полосами антискольжения</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="img/photo/img_oktagon02.jpg" class="d-block w-100 rounded-lg border border-dark" alt="...">
                            <div class="carousel-caption d-none d-md-block text-dark">
                                <h5>Октагон</h5>
                                <p>Плитка восьмиугольной формы придаст роскошный английский стиль интерьеру</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="img/photo/img03.jpg" class="d-block w-100 rounded-lg border border-dark" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Умывальник из керамогранита</h5>
                                <p></p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="img/photo/img05.jpg" class="d-block w-100 rounded-lg border border-dark" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Модули наружного угла</h5>
                                <p></p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="img/photo/img06.jpg" class="d-block w-100 rounded-lg border border-dark" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Столешница под умывальник</h5>
                                <p></p>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselPhoto" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselPhoto" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
            </div>

            <div class="card flex-box bg-dark mw50">
                <div class="row no-gutters" style="flex-flow:row nowrap">
                    <div class="col-md-4">
                        <img src="img/photo/img_stellaj.jpg" class="card-img" alt="...">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title text-yellow">Размер не имеет значение <span class="badge badge-danger">new</span></h5>
                            <p class="card-text text-white">Производители керамических плит не могут сдержаться и уже выпускают максимальный размер 3,2 метра. Мы не отстаем от предлагаемых новинок и теперь режем плиты неограниченных исходных размеров.</p>
                            <p class="card-text"><small class="text-muted">29/11/2019</small></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="p-2 text-center">
            <a id="rezka"></a>
            <div class="btn btn-dark text-yellow btn-lg">Прямой рез</div>
        </div>
        <div class="card card-body bg-grey">
            <p class="lead">Осуществляем прямой рез керамической плитки, керамогранита, мрамора и других подобных материалов любых исходных размеров по замерам заказчика быстро, качественно, без сколов и пыли на оборудовании собственного производства с последующей обработкой краев (снятие фаски, полировка).</p>
            <div class="flex-box">
                <img class="shadow m-1 rounded mw300" src="img/photo/img01.jpg">
                <img class="shadow m-1 rounded mw300" src="img/photo/img_rezka01.jpg">
                <img class="shadow m-1 rounded mw300" src="img/photo/img04.jpg">
            </div>
        </div>

        <div class="p-2 text-center">
            <a id="pod45"></a>
            <div class="btn btn-dark text-yellow btn-lg">Фрезеровка под 45<sup>о</sup></div>
        </div>
        <div class="card card-body bg-yellow">
            <p class="lead">Фрезеровка плитки под углом 45 градусов. Качественно и точно, без сколов и царапин. <br> Используется для изготовления модулей наружного угла (плинтусов, подоконников, ступеней, откосов, столешниц и пр.)</p>
            <div class="flex-box">
                <img class="shadow m-1 rounded mw300" src="img/photo/img_podmy01.jpg">
                <img class="shadow m-1 rounded mw300" src="img/photo/img_podmy02.jpg">
                <img class="shadow m-1 rounded mw300" src="img/photo/img_podmy03.jpg">
            </div>
        </div>

        <div class="p-2 text-center">
            <a id="stup"></a>
            <div class="btn btn-dark text-yellow btn-lg">Ступени</div>
        </div>
        <div class="card card-body">
            <p class="lead">Изготавливаем ступени для лестниц любой сложности. <br>Возможно нанесение полос антискольжения (надежно и долговечно в отличие от накладок и клейких полос). <br>Капинос ступени придает лестнице благородный стиль, а изготовление их из той же плитки обеспечивает комплексное решение по отделке лестниц, включая подступенки и плинтуса.</p>
            <div class="flex-box">
                <img class="shadow m-1 rounded mw300" src="img/photo/img_stupeni.jpg">
                <img class="shadow m-1 rounded mw300" src="img/photo/img_stupeni02.jpg">
                <img class="shadow m-1 rounded mw300" src="img/photo/img_stup03.jpg">
            </div>
            <div class="text-center mt-2"><img src="img/icons-dostavka.png"> Выезд мастера  <img src="img/icons-zamer.png"> Консультация, замеры, расчет </div>
        </div>
        <div class="p-2 text-center">
            <a id="modul"></a>
            <div class="btn btn-dark text-yellow btn-lg">Модули наружного угла</div>
        </div>
        <div class="card card-body bg-grey">
            <p class="lead">Использование пластиковых уголков и декоративных накладок в наружном угле плитки не комильфо. Фрезеровка торца плитки под 45 градусов позволяет эстетично соединить плитку в любом модуле (углы стен, душевые поддоны, ступени, полки и т.п.)</p>
            <div class="flex-box">
                <img class="shadow m-1 rounded mw300" src="img/photo/img05.jpg">
                <img class="shadow m-1 rounded mw300" src="img/photo/img_pod02.jpg">
            </div>
        </div>
        <div class="p-2 text-center">
            <a id="stol"></a>
            <div class="btn btn-dark text-yellow btn-lg">Столешницы</div>
        </div>
        <div class="card card-body bg-yellow">
            <p class="lead">Наше оборудование позволяет резать плиты крупного формата и изготовить подоконники и столешницы по любому эскизу заказчика. Такие изделия выглядят основательно и практичны в использовании.</p>
            <div class="flex-box">
                <img class="shadow m-1 rounded mw300" src="img/photo/img03.jpg">
                <img class="shadow m-1 rounded mw300" src="img/photo/img06.jpg">
                <img class="shadow m-1 rounded mw300" src="img/photo/img_stol01.jpg">
            </div>
             <div class="flex-box">
                <img class="shadow m-1 rounded mw300" src="img/photo/img_um03.jpg">
                <img class="shadow m-1 rounded mw300" src="img/photo/img_stolmy04.jpg">
            </div>
            <div class="text-center mt-2"><img src="img/icons-dostavka.png"> Выезд мастера  <img src="img/icons-zamer.png"> Консультация, замеры, расчет </div>
        </div>
        <div class="p-2 text-center">
            <a id="otv"></a>
            <div class="btn btn-dark text-yellow btn-lg">Отверстия</div>
        </div>
        <div class="card card-body">
            <p class="lead">Под смеситель, розетку, полотенцесушитель, декоративные элементы необходимо просверлить отверстия? Мастер с выездом к заказчику в уже приклееной плитке специальными сверлами с алмазным напылением сделает отверстия (с шумом и пылью) в указанных местах. 
            &empty; 6, 8, 10, 12мм   &empty; 20-30мм  &empty; 40-80мм <br> Также осуществляем вырез любых форм - под трап, инсталляции и т.д.</p>
            <div class="flex-box">
                <img class="shadow m-1 rounded mw300" src="img/photo/img_otvmy02.jpg">
                <img class="shadow m-1 rounded mw300" src="img/photo/img_otv02.jpg">
                <img class="shadow m-1 rounded mw300" src="img/photo/img_otv01.jpg">
            </div>
            <div class="flex-box">
                <img class="shadow m-1 rounded mw300" src="img/photo/img_otvmy01.jpg">
                <img class="shadow m-1 rounded mw300" src="img/photo/img_otvmy04.jpg">
                <img class="shadow m-1 rounded mw300" src="img/photo/img_otvmy03.jpg">
            </div>
            <div class="text-center mt-2"><img src="img/icons-dostavka.png"> Выезд мастера </div>
        </div>
        <div class="p-2"></div>
        <div class="flex-box">
            <div class="card flex-box bg-dark mw50 mr-1">
                <div class="row no-gutters" style="flex-flow:row nowrap">
                    <div class="col-md-4">
                        <img src="img/photo/img_kamin.jpg" class="card-img" alt="Камин">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title text-yellow">Камин в жилом доме</h5>
                            <p class="card-text text-white">Изготовлен из натурального камня "Змеевик" с закруглением фаски и округлением верхнего края кромки</p>
                            <p class="card-text"><small class="text-muted">16/05/2019</small></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card flex-box bg-dark mw50">
                <div class="row no-gutters" style="flex-flow:row nowrap">
                    <div class="col-md-4">
                        <img src="img/photo/img_meteorit.jpg" class="card-img" alt="Метеорит">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title text-yellow">Метеорит в Солигорске <span class="badge badge-warning">cool</span></h5>
                            <p class="card-text text-white">Авторский проект Виктора Николаева изготовлен в нашей мастерской. В скульптуре использованы слэбы натурального оникса, листы нержавеющей стали, система внутренней подсветки.</p>
                            <p class="card-text"><small class="text-muted">02/10/2019</small></p>
                        </div>
                    </div>
                </div>
            </div>
        
        </div>
        <div class="p-2"></div>
		<jsp:include page="footer.jsp"></jsp:include>
	</div>
	<script type="text/javascript" src="<c:url value="/js/jquery.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/js/bootstrap.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/js/bootbox.min.js"/>"></script>
</body>
</html>
