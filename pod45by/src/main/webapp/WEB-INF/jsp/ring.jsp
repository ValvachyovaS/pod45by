<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html lang="ru">
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css"/>" />
<link rel="stylesheet" href="<c:url value="/css/styles.css"/>" />
<title>pod45.by Звонок</title>
<link rel="shortcut icon" type="image/png" href="<c:url value="/img/favicon.png"/>"/>
</head>
<body>
	<div class="container">
	<jsp:include page="navbar.jsp"></jsp:include>
	<div class="container-item mw500">
	<h3>Заказать звонок:</h3>
	<p class="lead">Введите данные, нажмите "Отправить". Мастер перезвонит Вам в ближайшее время.</p>
		<form method="POST" action="" id="forma" onsubmit="sendRing()">
			<div class="form-group">
					<label for="log-tel1">Телефон:</label>
					<div class="input-group">
						<span class="input-group-text">+375 (</span> 
						<input type="text" name="tel1" id="log-tel1" maxlength="2" required pattern="[0-9]{2,}" 
						title="Код мобильного оператора: 25 29 33 44" size="2" value="${valueTel1}"> 
						<span class="input-group-text">) </span> 
						<input class="icons" type="text" name="tel2" id="log-tel2" maxlength="7" required
							pattern="[0-9]{7,}" title="Телефон в формате +375 (хх) ххххххх" size="7" value="${valueTel2}">
					</div><br>
					<label for="ring-name">Имя:</label> 
					<input type="text" name="nam" id="ring-name" maxlength="50"
						required pattern="[a-zA-Zа-яА-ЯёЁ -.]{2,50}" value="${valueNam}"
						class="icons" title="Латинские и кириллица от 2 до 50 символов">	
			</div>
			<p class="lead" style="color:red" id="ringmsg">${msgRing}</p>
			<button type="submit" class="btn btn-success">Отправить</button>
			<a class="btn btn-primary" href="<c:url value="/index.html"/>"
				role="button">На главную</a> <br>
			<hr class="my-4">
		</form>
	</div>
	<jsp:include page="footer.jsp"></jsp:include>
	</div>
	<script type="text/javascript" src="<c:url value="/js/jquery.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/js/bootstrap.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/js/bootbox.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/js/scripts.js"/>"></script>
	<script>
	function sendRing() {
		document.getElementById('ringmsg').innerHTML = "Отправка сообщения...";
	}
	document.getElementById('ring-tel1').addEventListener('focus', function () {
	     document.getElementById('ringmsg').innerHTML = "";
	 });
	document.getElementById('ring-tel2').addEventListener('focus', function () {
	     document.getElementById('ringmsg').innerHTML = "";
	 });
	document.getElementById('ring-name').addEventListener('focus', function () {
	     document.getElementById('ringmsg').innerHTML = "";
	 });
	</script>
</body>
</html>