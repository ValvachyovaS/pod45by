<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true" errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE HTML>
<html lang="ru">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css"/>" />
<link rel="stylesheet" href="<c:url value="/css/styles.css"/>" />
<title>pod45.by Регистрация</title>
</head>
<link rel="shortcut icon" type="image/png" href="<c:url value="/img/favicon.png"/>"/>
<body>
	<div class="container">
	<jsp:include page="navbar.jsp"></jsp:include>
	<div class="container-item mw500">
	<h3>Регистрация пользователя:</h3>
	<p class="lead">Заполните все поля необходимыми данными, код с картинки и нажмите "Зарегистрироваться" </p>
		  <form method="POST" id="regform">
			<table>
			<tr><td><label>Телефон:</label></td>
			<td><div class="input-group">
						<span class="input-group-text">+375 (</span> 
						<input type="text" name="tel1" id="tel1" maxlength="2" required pattern="[0-9]{2,}" 
						title="Код мобильного оператора: 25 29 33 44" size="2" value="${valueTel1}"> 
						<span class="input-group-text">) </span> 
						<input class="icons" type="text" name="tel2" id="tel2" maxlength="7" required
							pattern="[0-9]{7,}" title="Телефон в формате +375 (хх) ххххххх" size="7" value="${valueTel2}">
					</div></td></tr>
			<tr><td colspan="2"><p class="lead" style="color:red" id="errTel">${errTelefon}</p></td></tr>
			<tr><td><label for="pas">Пароль:</label></td> 
				<td><input type="password" name="pas" id="pas" autocomplete="off" maxlength="9" required pattern="[0-9a-zA-Z]{5,9}" 
					title="Латинские буквы и цифры от 5 до 9 символов" class="icons" size="9"  value="${fn:escapeXml(param.pas)}"></td></tr>
			<tr><td><label for="pas2">Повторите пароль:</label></td> 
			<td><input type="password" name="pas2" id="pas2" autocomplete="off"
					maxlength="9" required pattern="[0-9a-zA-Z]{5,9}" title="Пароли должны совпадать" 
					class="icons" size="9" value="${fn:escapeXml(param.pas2)}"></td></tr>
			<tr><td colspan="2"><p class="lead" style="color:red" id="errPas">${errPassword}</p></td></tr>
					<tr><td><label for="nam">Имя:</label></td> <td> <input type="text" name="nam" id="nam" maxlength="20"
						pattern="[a-zA-Zа-яА-Я\s]{2,40}" required class="icons" size="20" value="${valueNam}">	
					</td></tr>
					<tr><td><label for="eml">Электронный адрес:</label> </td> <td><input type="email" name="eml" id="eml" 
					maxlength="40" required placeholder="example@mail.com" class="icons" size="30" 
					title="Ящик нужен для доп.средства связи и восстановления пароля" value="${valueEml}">	
					</td></tr>
					<tr><td><label for="code">*Код с картинки:</label></td> <td><input type="text" name="code" id="code" 
					required autocomplete="off" class="icons" title="Латинские строчные буквы и цифры с картинки">
					</td></tr> 
					<tr><td colspan="2"><p class="lead" style="color:red" id="errCap">${errCaptcha}</p></td></tr>
					<tr><td></td> <td><img src="ic.jpg" id="cap"><br><a href="#" onclick="refreshCaptcha();" 
					title="Загрузить другую картинку">Обновить</a>
					</td></tr>			
		</table> 
		<br> <p class="lead" style="color:red" id="errRegister">${errRegister}</p>
			 <input type="submit" class="btn btn-success" value="Зарегистрироваться">  
			<input type="reset" class="btn btn-success" value="Очистить">
			<a class="btn btn-primary" href="<c:url value="/index.html"/>" role="button">На главную</a> <br>
			<a href="<c:url value="/ring.html"/>" title="Перенаправление на страницу заказа звонка">Не получается</a>
			<hr class="my-4">
		</form>
	</div>
	<jsp:include page="footer.jsp"></jsp:include>
	</div>
	<script>
	function refreshCaptcha() {
		document.getElementById('errCap').innerHTML = "";
		document.getElementById('cap').src='ic.jpg?'+Math.random();
		var code = document.getElementById('code');
		code.value = "";
		code.focus();
	}
	 document.getElementById('pas').addEventListener('focus', function () {
	     document.getElementById('errPas').innerHTML = "";
	 });
	 document.getElementById('pas2').addEventListener('focus', function () {
	     document.getElementById('errPas').innerHTML = "";
	 });
	 document.getElementById('tel1').addEventListener('focus', function () {
	     document.getElementById('errTel').innerHTML = "";
	 });
	 document.getElementById('tel2').addEventListener('focus', function () {
	     document.getElementById('errTel').innerHTML = "";
	 });
	 document.getElementById('code').addEventListener('focus', function () {
	     document.getElementById('errCap').innerHTML = "";
	 });
	</script>
</body>
</html>