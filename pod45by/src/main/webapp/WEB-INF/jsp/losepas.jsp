<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true"
	errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html lang="ru">
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css"/>" />
<link rel="stylesheet" href="<c:url value="/css/styles.css"/>" />
<title>pod45.by Восстановление пароля</title>
<link rel="shortcut icon" type="image/png" href="<c:url value="/img/favicon.png"/>"/>
</head>
<body>
	<div class="container">
		<jsp:include page="navbar.jsp"></jsp:include>
		<div class="container-item mw500">
			<h3>Восстановление пароля:</h3>
			<p class="lead">
				Введите номер телефона, указанный при регистрации, и нажмите
				"Отправить". <br> На почтовый ящик придет сообщение (проверьте
				папку Спам).
			</p>
			<form method="POST" action="" id="losepas-forma" onsubmit="sendPas()">
				<div class="form-group">
					<label>Телефон:</label>
					<div class="input-group">
						<span class="input-group-text">+375 (</span> <input type="text"
							name="tel1" id="losepas-tel1" maxlength="2" required
							pattern="[0-9]{2,}" title="Код мобильного оператора: 25 29 33 44"
							size="2"> 
							<span class="input-group-text">) </span> 
							<input class="icons" type="text" name="tel2" id="losepas-tel2" 
							maxlength="7" required pattern="[0-9]{7,}" 
							title="Телефон в формате +375 (хх) ххххххх" size="7">
					</div>
					<p class="lead" style="color: red" id="losepas-msg">${msgLosepas}</p>
					<button type="submit" class="btn btn-success">Отправить</button>
					<a class="btn btn-primary" href="<c:url value="/index.html"/>"
						role="button">На главную</a> <br>
				</div>
			</form>
			<br>
		</div>
		<jsp:include page="footer.jsp"></jsp:include>
	</div>
	<script>
		function sendPas() {
			document.getElementById('losepas-msg').innerHTML = "Отправка сообщения...";
		}
	</script>
</body>
</html>