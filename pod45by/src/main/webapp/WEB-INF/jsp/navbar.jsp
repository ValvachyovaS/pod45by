<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true"
	errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<style type="text/css">
	.top-menu {
	    display: flex;
	    flex-flow: row wrap;
	    justify-content: space-between;
	    align-items: center;
	    width: 100%;
	    min-height: 100px;
	    color: rgb(255, 255, 255);
	    background-image: url(<c:url value="img/bg-stone.jpg"/>);
	    background-repeat: repeat;
	    border-radius: 2px;
	    padding: 20px;
	}
	
	.text-gradient {
	    text-decoration: none;
	    background: linear-gradient(rgb(255, 255, 255), rgb(102, 102, 102));
	    -webkit-background-clip: text;
	    color: transparent;
	    font-weight: bold;
	    transition: background 10s ease-out;
	    -webkit-transition: background 10s ease-out;
	}
	
	.text-gradient:hover {
	    text-decoration: none;
	    background: linear-gradient(rgb(102, 102, 102), rgb(255, 255, 255));
	    -webkit-background-clip: text;
	    color: transparent;
	    font-weight: bold;
	    transition: background 10s ease-out;
	    -webkit-transition: background 10s ease-out;
	}
	
	.movebox {
	    animation-name: movingBox;
	    animation-duration: 5000ms;
	    animation-iteration-count: infinite;
	    animation-direction: alternate;
	}
	
	@keyframes movingBox {
	    0% {
	        transform: translate(0px, -18px);
	    }
	
	    50% {
	        transform: translate(228px, -18px);
	    }
	
	    100% {
	        transform: translate(0px, -18px);
	    }
	}
</style>
<div class="top-menu">
			<a href="<c:url value="/index.html#top"/>" class="h1 text-gradient"><img class="movebox" src="img/icons8-saw.png">pod45.by</a>
            <p class="lead">Мастерская оказания услуг<br> по обработке плитки в Витебске</p>
            <a class="btn btn-warning btn-lg m-2 hidden" href="tel:+375292176715" role="button">
                <img src="img/icon-mob.png">Позвонить</a>
	<a class="btn btn-dark btn-lg mb-2" href="<c:url value="/order.html"/>"
		role="button" title="Перейти к оформлению заявки на обработку плитки">
		<img src="<c:url value="/img/icon-doc.png"/>">Оформить заявку</a>
	<a class="btn btn-dark btn-lg mb-2" href="<c:url value="/ring.html"/>"
		role="button"> <img src="<c:url value="/img/icon-ring.png"/>">Заказать звонок</a>
</div>

<nav class="navbar navbar-expand-sm navbar-dark bg-dark">
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span></button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
		
		<li class="nav-item"><a class="nav-link text-warning" href="<c:url value="/about.html"/>">Мастерская</a></li>
        <li class="nav-item dropdown">
        	<a class="nav-link dropdown-toggle text-warning" href="<c:url value="/index.html#top"/>" id="navbarDropdown" role="button" 
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Услуги</a>
                        <div class="dropdown-menu bg-warning" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="<c:url value="/index.html#rezka"/>">Прямой рез</a>
                            <a class="dropdown-item" href="<c:url value="/index.html#pod45"/>">Фрезеровка под 45<sup>о</sup></a>
                            <a class="dropdown-item" href="<c:url value="/index.html#stup"/>">Ступени</a>
                            <a class="dropdown-item" href="<c:url value="/index.html#modul"/>">Модули наружного угла</a>
                            <a class="dropdown-item" href="<c:url value="/index.html#stol"/>">Столешницы</a>
                            <a class="dropdown-item" href="<c:url value="/index.html#otv"/>">Отверстия</a>
                        </div>
                    </li>
                    
			<c:if test="${empty sessionScope.reguser}">
				<li class="nav-item"><a class="nav-link"
					href="<c:url value="/login.html"/>">Войти</a></li>
				<li class="nav-item"><a class="nav-link"
					href="<c:url value="/register.html"/>">Зарегистрироваться</a></li>
			</c:if>
			<c:if test="${not empty sessionScope.reguser}">
				<li class="nav-item"><div class="navbar-brand"> 
					<img src="<c:url value="/img/icon-user.png"/>" class="d-inline">
					${sessionScope.reguser.getNam()}</div></li>
				<li class="nav-item"><a class="nav-link"
					href="<c:url value="/account.html"/>">Профиль</a></li>
				<c:if test="${sessionScope.reguser.numRol() == 9}">
					<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="<c:url value="/admin.html"/>" 
						id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
						aria-expanded="false">Админ</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="<c:url value="/admin-order.html"/>">Заявки</a> 
							<a class="dropdown-item" href="<c:url value="/admin-user.html"/>">Пользователи</a>
							<a class="dropdown-item" href="<c:url value="/admin-service.html"/>">Услуги</a> 
							<a class="dropdown-item" href="<c:url value="/admin-ring.html"/>">Звонки</a>
						</div></li>
				</c:if>
				<li class="nav-item"><a class="nav-link" id="btn-getout" 
					href="<c:url value="/getout.html"/>">Выйти</a></li>
			</c:if>
		</ul>
	</div>
	<div class="navbar-brand"> +375 (29) 217-67-15 <img src="<c:url value="/img/icon-mts.png"/>"
		class="d-inline" alt="МТС"> <img src="<c:url value="/img/icon-viber.png"/>" class="d-inline" alt="Viber">
	</div>
</nav>
<script type="text/javascript">
	var getoutBtn = document.getElementById("btn-getout");
	if (getoutBtn) {
		getoutBtn.addEventListener("click", function(e) {
			e.preventDefault();
			bootbox.confirm({
				message : "Выйти?",
				buttons : {
					confirm : {
						label : 'Да',
						className : 'btn-success'},
					cancel : {
						label : 'Нет',
						className : 'btn-light'}},
				callback : function(result) {
					if (result) {
						location.href = "<c:url value="/getout.html"/>";}
				}
			});
		});
	}
</script>
