<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true"
	errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE HTML>
<html lang="ru">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css"/>" />
<link rel="stylesheet" href="<c:url value="/css/styles.css"/>" />
<title>pod45.by Управление пользователями</title>
<link rel="shortcut icon" type="image/png" href="<c:url value="/img/favicon.png"/>" />
</head>
<body>
	<div class="container">
		<jsp:include page="navbar.jsp"></jsp:include>
		<div class="container-item mw1000">
			<h3>Список пользователей:</h3>
			<p class="lead msg-error" id="admin-user-msg">${msgAdminUser}</p>
			<p class="lead">Поиск по телефону:</p>
			<form method="POST" action="" id="form-search-user">
				<div class="input-group">
					<span class="input-group-text">+375 (</span> <input type="text"
						name="tel1" id="admin-user-tel1" maxlength="2" required
						pattern="[0-9]{2,}" title="Код мобильного оператора: 25 29 33 44"
						size="2"> <span class="input-group-text">) </span> <input
						class="icons" type="text" name="tel2" id="admin-user-tel2"
						maxlength="7" required pattern="[0-9]{7,}"
						title="Телефон в формате +375 (хх) ххххххх" size="7">
					<button type="submit" class="btn btn-success">Найти</button>
					<a class="btn btn-dark" href="<c:url value="/admin-user.html"/>"
						role="button">Показать все</a>
				</div>
			</form>
			<br>
			<p class="lead">Отметьте пользователя и выберите нужное действие:
			</p>
			<form id="admin-user-forma">
				<button class="btn btn-secondary" name="submit-btn"
					value="admin" title="Установить роль администратора с возможностями управления системой">Назначить администратором</button>
				<button class="btn btn-info" name="submit-btn"
					value="user" title="Установить роль пользователя">Назначить пользователем</button>
				<button class="btn btn-warning" name="submit-btn"
					value="lock" title="Заблокировать пользователяю Вход в систему для него будет невозможен">Заблокировать</button>
				<button class="btn btn-danger" name="submit-btn" id="btn-delete-user"
					value="delete" title="Удалить пользователя безвозвратно со всеми его заявками">Удалить</button>
			</form>
			<br>
			<table class="table table-secondary table-striped table-sm"
				id="table-user">
				<thead>
					<tr>
						<th scope="col"></th>
						<th scope="col">Id</th>
						<th scope="col">Дата, время рег-ции</th>
						<th scope="col">Телефон</th>
						<th scope="col">Имя</th>
						<th scope="col">E-mail</th>
						<th scope="col">Роль</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${listUser}" var="user">
						<tr>
							<td>
						<div class="custom-control custom-switch custom-control-inline">
						<input type="radio" form="admin-user-forma" class="custom-control-input" 
						id="user${user.getId()}" name="user-id" value="${user.getId()}">
						<label class="custom-control-label" for="user${user.getId()}"> </label>
						</div>
							<td>${user.getId()}</td>
							<td>${fn:substring(user.datime, 0, 16)}</td>
							<td>+375 ${user.getTel()}</td>
							<td>${user.getNam()}</td>
							<td>${user.getEml()}</td>
							<td>${user.getRol()}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<br>
		</div>
		<jsp:include page="footer.jsp"></jsp:include>
	</div>
	<script type="text/javascript" src="<c:url value="/js/jquery.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/js/bootstrap.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/js/bootbox.min.js"/>"></script>
	<script type="text/javascript">
	<%--Очищение меседжа при нажатии на переключатели --%>
	$(document).on('click', '[name="user-id"]', function () {
		document.getElementById('admin-user-msg').innerHTML = "";
	});
	<%-- Подтверждающее окно при действии с пользователями (удалении)--%>
	$(document).on('click', '[name="submit-btn"]', function (e) {
		var valBtn = this.value;
		if (valBtn == "delete"){
			e.preventDefault();
		bootbox.confirm({
		    message: "Вы уверены, что хотите удалить пользователя с его заявками?",
		    buttons: {
		        confirm: {
		            label: 'Да',
		            className: 'btn-success'},
		        cancel: {
		            label: 'Нет',
		            className: 'btn-light'}},
		    callback: function (result) {
		        if (result){
		        	var formUser = document.getElementById("admin-user-forma");
		        	formUser.innerHTML = '<input name="submit-btn" value="delete">';
		        	formUser.submit();
		        }
		    }
        });
		}
	});
	</script>
</body>
</html>