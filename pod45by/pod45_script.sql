CREATE USER 'user'@'%' IDENTIFIED VIA mysql_native_password USING 'user';
GRANT ALL PRIVILEGES ON *.* TO 'user'@'%' REQUIRE NONE 
WITH GRANT OPTION MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;

CREATE DATABASE IF NOT EXISTS `pod45` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `pod45`;

CREATE TABLE `rings` (
  `id` int(11) NOT NULL,
  `datime` datetime NOT NULL,
  `tel` varchar(9) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `rings` (`id`, `datime`, `tel`, `name`) VALUES
(1, '2019-10-20 17:14:44', '298764534', 'Cbeta'),
(2, '2019-10-21 18:04:50', '292900000', 'test'),
(3, '2019-10-26 15:14:52', '331234567', 'new'),
(4, '2019-10-28 15:37:49', '332900000', 'Eugene'),
(10, '2019-12-11 10:14:22', '298166524', 'Svetlana');

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `datime` datetime NOT NULL,
  `tel` varchar(9) NOT NULL,
  `pas` varchar(24) NOT NULL,
  `nam` varchar(20) NOT NULL,
  `eml` varchar(40) NOT NULL,
  `rol` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `users` ADD UNIQUE( `tel`);

INSERT INTO `users` (`id`, `datime`, `tel`, `pas`, `nam`, `eml`, `rol`) VALUES
(1, '2019-07-15 00:00:00', '292176715', '6gmEIXV9LfNFJs9XWiL4uA==', '�������������', '', 9),
(34, '2019-12-11 10:11:34', '298166524', '4BzWt2MADzp80mu0DHsgfA==', 'Svetlana', 'c-beta81@mail.ru', 1);

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `datime` datetime NOT NULL,
  `adr` varchar(50) DEFAULT '',
  `comment` varchar(200) DEFAULT '',
  `discount` int(11) NOT NULL DEFAULT 0,
  `status` varchar(10) NOT NULL DEFAULT '���������',
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `orders` (`id`, `datime`, `adr`, `comment`, `discount`, `status`, `user_id`) VALUES
(11, '2019-12-11 10:12:18', '������, 86', '��������� ���������� ����������� ����� ������� ������. ������ ��� �� ���������, ������� �������� ��������� � ������.', 0, '���������', 34),
(12, '2019-12-11 10:13:02', '��� ��������', '', 0, '���������', 34);

REATE TABLE `ordrows` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `count` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `ordrows` (`id`, `order_id`, `service_id`, `count`) VALUES
(20, 11, 1, 40),
(21, 11, 2, 12),
(22, 11, 7, 2),
(23, 12, 4, 4);


CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `rate` decimal(6,2) NOT NULL,
  `act` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `services` (`id`, `name`, `rate`, `act`) VALUES
(1, '������ ��� (1� ���.)', '5.50', 1),
(2, '���������� ����� ������ ��� 45 �������� (1� ���.)', '7.00', 1),
(3, '������ ����� (����������� �����) ��� �������� � �������� (1� ���.)', '3.50', 1),
(4, '��������� 3-� ����� �������������� (1� ���.)', '4.50', 1),
(5, '������������ ������� ��������� ���� (1� ���.)', '8.50', 1),
(6, '������������ ��������� ������� 6, 8, 10, 12 (1 ��.)', '4.00', 1),
(7, '������������ ��������� ������� 20-30 (1 ��.)', '8.00', 1),
(8, '������������ ��������� ������� 40-80 (1 ��.)', '10.00', 1),
(11, '��������� 3-� ����� �������������� (1� ���.)', '10.00', 0),
(12, '���������� ����� ������ ��� 45 �������� (1� ���.)', '10.00', 0);


-- ������� ������� `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_user_orders` (`user_id`);

--
-- ������� ������� `ordrows`
--
ALTER TABLE `ordrows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_order_ordrows` (`order_id`),
  ADD KEY `FK_service_ordrows` (`service_id`);

--
-- ������� ������� `rings`
--
ALTER TABLE `rings`
  ADD PRIMARY KEY (`id`);

--
-- ������� ������� `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- ������� ������� `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tel` (`tel`);

--
-- AUTO_INCREMENT ��� ������� `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT ��� ������� `ordrows`
--
ALTER TABLE `ordrows`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT ��� ������� `rings`
--
ALTER TABLE `rings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT ��� ������� `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT ��� ������� `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;


-- ����������� �������� ����� ������� `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `FK_user_orders` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- ����������� �������� ����� ������� `ordrows`
--
ALTER TABLE `ordrows`
  ADD CONSTRAINT `FK_order_ordrows` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `FK_service_ordrows` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`);